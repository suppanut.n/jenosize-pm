<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title></title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <!-- HOME CAROUSEL -->
      <section class="section section-home-carousel">
        <!-- TAG H1 FOR SEO -->
        <div class="section-title ui-hidden">
          <h1>Prime Minister’s Export Award</h1>
        </div>
        <div class="home-carousel">
          <div class="home-carousel-slider swiper-container">
            <div class="swiper-wrapper">
              <!-- 1 -->
              <div class="swiper-slide">
                <div class="home-carousel-bg">
                  <div class="figure">
                    <div class="figure-wrap" style="background-image: url(assets/images/home/carousel/banner-01.jpg)">
                      <img src="assets/images/home/carousel/banner-01.jpg" alt="">
                    </div>
                  </div>
                </div>
                <div class="home-carousel-content">
                  <div class="fixed-layout">
                    <div class="section-outer">
                      <div class="home-carousel-content-logo">
                        <span data-swiper-parallax="-300" data-swiper-parallax-opacity="0">
                          <img src="assets/images/home/carousel/logo.svg" alt="Prime Minister’s Export Award">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- 2 -->
              <div class="swiper-slide">
                <div class="home-carousel-bg">
                  <div class="figure">
                    <div class="figure-wrap" style="background-image: url(assets/images/home/carousel/banner-01.jpg)">
                      <img src="assets/images/home/carousel/banner-01.jpg" alt="">
                    </div>
                  </div>
                </div>
                <div class="home-carousel-content">
                  <div class="fixed-layout">
                    <div class="section-outer">
                      <div class="home-carousel-content-logo">
                        <span data-swiper-parallax="-300" data-swiper-parallax-opacity="0">
                          <img src="assets/images/home/carousel/logo.svg" alt="Prime Minister’s Export Award">
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Add Pagination -->
            <div class="swiper-pagination home-carousel-pagination"></div>
          </div>
        </div>
      </section>
      <!-- HOME INTRO -->
      <section class="section section-home-intro">
        <div class="home-intro-bg"></div>
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="home-intro">
              <div class="home-intro-row">
                <div class="home-intro-col">
                  <div class="home-intro-content">
                    <div class="home-intro-detail is-revealing">
                      <div class="home-intro-title">
                        <h2 class="text-heading-bold text-uppercase">Prime Minister’s Export Award</h2>
                      </div>
                      <div class="home-intro-desc">
                        <p>เป็นรางวัลสูงสุดของรัฐบาลที่มอบให้แก่ผู้ประกอบธุรกิจดีเด่น
                          เพื่อแสดงถึงภาพลักษณ์ของคุณภาพและมาตรฐานของสินค้าไทยในตลาดโลก</p>
                      </div>
                    </div>
                    <div class="home-intro-decorate is-revealing">
                      <span class="icon-svg">
                        <canvas width="359px" height="942px"></canvas>
                        <img data-src="assets/images/home/pm-award/decorate.png" alt="" class="js-imageload-self">
                      </span>
                    </div>
                  </div>
                </div>
                <div class="home-intro-col">
                  <div class="home-intro-lists is-revealing">
                    <!-- 1 -->
                    <div class="home-intro-list">
                      <div class="home-intro-list-content">
                        <div class="home-intro-list-icon">
                          <img data-src="assets/images/home/intro/icon-stats.svg" alt="" class="js-imageload-self">
                        </div>
                        <div class="home-intro-list-title">
                          <h3 class="h5"><strong>ยกระดับภาพลักษณ์ประเทศด้วยสินค้า</strong> และบริการของไทย</h3>
                        </div>
                      </div>
                    </div>
                    <!-- 2 -->
                    <div class="home-intro-list">
                      <div class="home-intro-list-content">
                        <div class="home-intro-list-icon">
                          <img data-src="assets/images/home/intro/icon-people.svg" alt="" class="js-imageload-self">
                        </div>
                        <div class="home-intro-list-title">
                          <h3 class="h5"><strong>สร้างกระแสบริโภคนิยมสินค้าไทย</strong> ให้เกิดขึ้น ในหมู่ชาวโลก</h3>
                        </div>
                      </div>
                    </div>
                    <!-- 3 -->
                    <div class="home-intro-list">
                      <div class="home-intro-list-content">
                        <div class="home-intro-list-icon">
                          <img data-src="assets/images/home/intro/icon-reputation.svg" alt="" class="js-imageload-self">
                        </div>
                        <div class="home-intro-list-title">
                          <h3 class="h5"><strong>ผู้ประกอบการไทยพัฒนารูปแบบ</strong> และคุณภาพสินค้า</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- HOME REGISTER -->
      <section class="section section-home-register">
        <!-- BG -->
        <div class="home-register-bg bg">
          <div class="bg-container">
            <img data-src="assets/images/home/register/bg.jpg" alt="" class="js-imageload">
          </div>
          <div class="bg-overlay"></div>
        </div>
        <div class="fixed-layout">
          <div class="section-outer">
            <!-- CONTENT -->
            <div class="home-register-content is-revealing">
              <div class="home-register-title">
                <h2 class="h1">Prime Minister’s Export Award</h2>
              </div>
              <div class="home-register-desc">
                <p>เป็นรางวัลสูงสุดของรัฐบาลที่มอบให้แก่ผู้ประกอบธุรกิจดีเด่น
                  เพื่อแสดงถึงภาพลักษณ์ของคุณภาพและมาตรฐานของสินค้าไทยในตลาดโลก</p>
              </div>
              <div class="home-register-botton">
                <a href="register.php" class="btn --gradient">REGISTER</a>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- HOME POWER UNDERRATE -->
      <section class="section section-home-power-underrate">
        <!-- BG -->
        <div class="home-power-underrate-bg bg">
          <div class="bg-container">
            <img data-src="assets/images/home/power-underrate/bg.jpg" alt="" class="js-imageload">
          </div>
          <div class="bg-overlay"></div>
        </div>
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="home-power-underrate-row">
                <!-- CONTENT -->
                <div class="home-power-underrate-content is-revealing">
                  <div class="home-power-underrate-title">
                    <h2 class="h1">Prime Minister’s Export Award Power Underrate</h2>
                  </div>
                  <div class="home-power-underrate-desc">
                    <p>เป็นรางวัลสูงสุดของรัฐบาลที่มอบให้แก่ผู้ประกอบธุรกิจดีเด่น
                      เพื่อแสดงถึงภาพลักษณ์ของคุณภาพและมาตรฐานของสินค้าไทยในตลาดโลก</p>
                  </div>
                </div>
                <!-- IMAGES -->
                <div class="home-power-underrate-images">
                  <div class="home-power-underrate-images-inner">
                    <!-- ITEMS -->
                    <div class="home-power-underrate-items is-revealing-inner">
                      <div class="home-power-underrate-item">
                        <div class="figure">
                          <div class="figure-wrap">
                            <img data-src="assets/images/home/power-underrate/img-01.jpg" alt="" class="js-imageload">
                          </div>
                        </div>
                      </div>
                      <div class="home-power-underrate-item">
                        <div class="figure">
                          <div class="figure-wrap">
                            <img data-src="assets/images/home/power-underrate/img-02.jpg" alt="" class="js-imageload">
                          </div>
                        </div>
                      </div>
                      <div class="home-power-underrate-item">
                        <div class="figure">
                          <div class="figure-wrap">
                            <img data-src="assets/images/home/power-underrate/img-03.jpg" alt="" class="js-imageload">
                          </div>
                        </div>
                      </div>
                      <div class="home-power-underrate-item">
                        <div class="figure">
                          <div class="figure-wrap">
                            <img data-src="assets/images/home/power-underrate/img-04.jpg" alt="" class="js-imageload">
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- HOME CAROUSEL VIDEO -->
      <section class="section section-home-carousel-video js-imageload-sec-wrap">
        <div class="home-carousel-video">
          <div class="home-carousel-video-slider">
            <!-- 1 -->
            <div class="home-carousel-video-slide js-lightbox js-gallery">
              <div class="home-carousel-video-bg">
                <div class="figure">
                  <div class="figure-wrap">
                    <img data-src="assets/images/home/carousel-video/bg-01.jpg" alt="" class="js-imageload-sec">
                  </div>
                </div>
                <div class="home-carousel-video-play btn-play is-revealing js-gallery--item" data-poster="assets/images/home/carousel-video/bg-01.jpg"
                  data-type="youtube" data-url="https://www.youtube.com/watch?v=ZGkQ9-mZM1Q">
                  <a href="" class="js-lightbox-link js-gallery--trigger">
                    <?php include('inc/icon-play.php'); ?></a>
                </div>
              </div>
              <div class="home-carousel-video-content">
                <div class="fixed-layout">
                  <div class="section-outer">
                    <div class="home-carousel-video-content-wrapper is-revealing">
                      <div class="home-carousel-video-content-title">
                        <h2 class="h1">PM AWARD 2018</h2>
                      </div>
                      <div class="home-carousel-video-content-date">
                        <h3 class="h4">Mar 27, 2018</h3>
                      </div>
                      <div class="home-carousel-video-content-desc">
                        <p>ปี ๒๕๓๕ ในสมัยนายกรัฐมนตรีอานันท์ ปันยารชุน กระทรวงพาณิชย์ไดจัดให้มีการพิจารณารางวัล
                          ผู้ส่งออกสินค้าและบริการดีเด่น (Prime Minister’s Export Award หรือ PM Export Award)
                          เป็นครั้งแรก
                          เพื่อแสดงถึงภาพลักษณ์ของคุณภาพและมาตรฐานของสินค้าไทยในตลาดโลกโดยมีเป้าหมายที่จะสนับสนุนและให้ความสำคัญแก่ผู้ส่งออกสินค้าและบริการที่มีผลงานดีเด่น
                          มีการริเริ่มและพยายามบุกเบิกตลาดต่างประเทศ</p>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <!-- 2 -->
            <div class="home-carousel-video-slide js-lightbox js-gallery">
              <div class="home-carousel-video-bg">
                <div class="figure">
                  <div class="figure-wrap">
                    <img data-src="assets/images/home/carousel-video/bg-01.jpg" alt="" class="js-imageload-sec">
                  </div>
                </div>
                <div class="home-carousel-video-play btn-play is-revealing js-gallery--item" data-poster="assets/images/home/carousel-video/bg-01.jpg"
                  data-type="youtube" data-url="https://www.youtube.com/watch?v=ZGkQ9-mZM1Q">
                  <a href="" class="js-lightbox-link js-gallery--trigger">
                    <?php include('inc/icon-play.php'); ?></a>
                </div>
              </div>
              <div class="home-carousel-video-content">
                <div class="fixed-layout">
                  <div class="section-outer">
                    <div class="home-carousel-video-content-wrapper">
                      <div class="home-carousel-video-content-title">
                        <h2 class="h1">PM AWARD 2018</h2>
                      </div>
                      <div class="home-carousel-video-content-date">
                        <h3 class="h4">Mar 27, 2018</h3>
                      </div>
                      <div class="home-carousel-video-content-desc">
                        <p>ปี ๒๕๓๕ ในสมัยนายกรัฐมนตรีอานันท์ ปันยารชุน กระทรวงพาณิชย์ไดจัดให้มีการพิจารณารางวัล
                          ผู้ส่งออกสินค้าและบริการดีเด่น (Prime Minister’s Export Award หรือ PM Export Award)
                          เป็นครั้งแรก
                          เพื่อแสดงถึงภาพลักษณ์ของคุณภาพและมาตรฐานของสินค้าไทยในตลาดโลกโดยมีเป้าหมายที่จะสนับสนุนและให้ความสำคัญแก่ผู้ส่งออกสินค้าและบริการที่มีผลงานดีเด่น
                          มีการริเริ่มและพยายามบุกเบิกตลาดต่างประเทศ</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- HOME NEWS EVENT -->
      <section class="section section-home-news js-imageload-sec-wrap">
        <div class="lines">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="home-news">
          <div class="home-news-headline is-revealing">
            <div class="fixed-layout">
              <div class="section-outer">
                <div class="home-news-headline-title">
                  <h2 class="h2 text-uppercase text-heading-bold">News & Event</h2>
                </div>
              </div>
            </div>
          </div>
          <div class="home-news-slider swiper-container is-revealing">
            <div class="swiper-wrapper">
              <?php
            $listApi = 'api/news-lists.json'; // path to your JSON file
            $listData = file_get_contents($listApi); // put the contents of the file into a variable
            $listArray = json_decode($listData); // deco√de the JSON feed
            foreach ($listArray as $value) {
            ?>
              <!-- <?php echo $value->id ?> -->
              <div class="swiper-slide">
                <a href="news-event-detail.php" class="home-news-box">
                  <!-- BG -->
                  <div class="home-news-bg">
                    <div class="figure">
                      <div class="figure-wrap">
                        <img data-src="<?php echo $value->home_thumb_url ?>" alt="" class="js-imageload-sec">
                      </div>
                      <div class="figure-overlay"></div>
                    </div>
                  </div>
                  <!-- DETAIL -->
                  <div class="home-news-detail">
                    <div class="home-news-detail-outer">
                      <div class="home-news-detail-title">
                        <h3 class="h2">
                          <?php echo $value->title ?>
                        </h3>
                      </div>
                      <div class="home-news-detail-date">
                        <p class="h4 text-uppercase">
                          <?php echo $value->date ?>
                        </p><span></span>
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <?php } ?>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next home-news-button-next">Next
              <?php include('inc/icon-arrow-right.php'); ?>
            </div>
            <div class="swiper-button-prev home-news-button-prev">
              <?php include('inc/icon-arrow-left.php'); ?> Prev</div>
          </div>
        </div>
      </section>
      <!-- HOME WINNER -->
      <section class="section section-home-winner js-imageload-sec-wrap">
        <div class="lines">
          <span></span>
          <span></span>
          <span></span>
        </div>
        <div class="home-winner">
          <div class="fixed-layout">
            <div class="section-outer">
              <div class="home-winner-row">
                <div class="home-winner-col">
                  <div class="home-winner-content">
                    <!-- DECORATE -->
                    <div class="home-winner-decorate is-revealing">
                      <span class="icon-svg">
                        <canvas width="359px" height="942px"></canvas>
                        <img data-src="assets/images/home/pm-award/decorate.png" alt="" class="js-imageload-self">
                      </span>
                    </div>
                    <!-- HEADLINE -->
                    <div class="home-winner-headline is-revealing">
                      <div class="home-winner-title">
                        <h2 class="h1">Pm Award 2018 (E-journals)</h2>
                      </div>
                      <div class="home-winner-desc">
                        <p>PM Award 2018 are the year's most prestigious awards for the highest-achieving
                          entrepreneurs.
                          A
                          Special Publication of the Bangkok Post </p>
                      </div>
                    </div>
                    <!-- TEXT -->
                    <div class="home-winner-text is-revealing">
                      <span class="icon-svg">
                        <canvas width="568px" height="79px"></canvas>
                        <img data-src="assets/images/home/pm-award/pm-award.svg" alt="" class="js-imageload-self">
                      </span>
                    </div>
                  </div>
                </div>
                <div class="home-winner-col">

                  <div class="home-winner-slider-wrapper is-revealing">
                    <div class="home-winner-slider swiper-container">
                      <div class="swiper-wrapper">
                        <?php
                    $listApi = 'api/winner-lists.json'; // path to your JSON file
                    $listData = file_get_contents($listApi); // put the contents of the file into a variable
                    $listArray = json_decode($listData); // decode the JSON feed
                    foreach ($listArray as $value) {
                    ?>
                        <!-- <?php echo $value->id ?> -->
                        <div class="swiper-slide">
                          <a href="" download class="home-winner-box">
                            <div class="home-winner-box-wrapper">
                              <!-- YEAR -->
                              <div class="home-winner-box-year">
                                <h3 class="h4">
                                  <?php echo $value->year ?>
                                </h3>
                              </div>
                              <!-- THUMB -->
                              <div class="home-winner-box-thumb">
                                <div class="figure">
                                  <div class="figure-wrap">
                                    <img data-src="<?php echo $value->bg_url ?>" alt="" class="js-imageload-sec">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                        </div>
                        <?php } ?>
                      </div>
                      <div class="swiper-pagination home-winner-pagination"></div>
                    </div>
                    <!-- Add Arrows -->
                    <div class="swiper-button-next home-winner-button-next">Next
                      <?php include('inc/icon-arrow-right.php'); ?>
                    </div>
                    <div class="swiper-button-prev home-winner-button-prev">
                      <?php include('inc/icon-arrow-left.php'); ?> Prev</div>
                      
                  </div>

                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('inc/cta.php'); ?>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <?php include('inc/modal-gallery.php'); ?>
  <script src="assets/scripts/main.js"></script>
</body>

</html>