<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title></title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <!-- NEWS DETAIL -->
      <section class="section section-news-detail js-imageload-sec-wrap">
        <div class="news-detail">
          <div class="news-detail-carousel">
            <div class="news-detail-carousel-slider swiper-container">
              <div class="swiper-wrapper">
                <!-- 1 -->
                <div class="swiper-slide">
                  <div class="news-detail-carousel-bg">
                    <div class="figure">
                      <div class="figure-wrap">
                        <img data-src="assets/images/news-event/slide-01.jpg" alt="" class="js-imageload-sec">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- 2 -->
                <div class="swiper-slide">
                  <div class="news-detail-carousel-bg">
                    <div class="figure">
                      <div class="figure-wrap">
                        <img data-src="assets/images/news-event/slide-01.jpg" alt="" class="js-imageload-sec">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- 3 -->
                <div class="swiper-slide">
                  <div class="news-detail-carousel-bg">
                    <div class="figure">
                      <div class="figure-wrap">
                        <img data-src="assets/images/news-event/slide-01.jpg" alt="" class="js-imageload-sec">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- 4 -->
                <div class="swiper-slide">
                  <div class="news-detail-carousel-bg">
                    <div class="figure">
                      <div class="figure-wrap">
                        <img data-src="assets/images/news-event/slide-01.jpg" alt="" class="js-imageload-sec">
                      </div>
                    </div>
                  </div>
                </div>
                <!-- 5 -->
                <div class="swiper-slide">
                  <div class="news-detail-carousel-bg">
                    <div class="figure">
                      <div class="figure-wrap">
                        <img data-src="assets/images/news-event/slide-01.jpg" alt="" class="js-imageload-sec">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- Add Pagination -->
              <div class="swiper-pagination news-detail-carousel-pagination"></div>
            </div>
          </div>
          <div class="news-detail-content">
            <div class="fixed-layout">
              <div class="section-outer">
                <div class="section-inner">
                  <div class="news-detail-content-box">
                    <!-- CONTENT -->
                    <div class="news-box-content is-revealing">
                      <div class="news-box-content-title">
                        <h1 class="h2">พิธีมอบรางวัลผู้ประกอบธุรกิจส่งออกดีเด่นประจำปี 2558</h1>
                      </div>
                      <div class="news-box-content-date">
                        <h2 class="h4 text-uppercase">13TH SEP 2018</h2>
                      </div>
                      <div class="news-box-content-desc">
                        <p>กรุงเทพฯ – นายกรัฐมนตรี พลเอกประยุทธ์ จันทร์โอชา
                          เป็นประธานในพิธีมอบรางวัลผู้ประกอบธุรกิจส่งออกดีเด่น (Prime Minister’s Export Award : PM
                          Award
                          2018) รับรองคุณภาพของผู้ประกอบการธุรกิจส่งออกไทย ให้เป็นที่ประจักษ์แก่สากล
                          ในปีนี้ได้ใช้แนวคิด
                          ‘Leading The Way’ สะท้อนถึงเส้นทางเดินแห่งความสำเร็จของนักธุรกิจไทย ซึ่งจัดโดย
                          สำนักส่งเสริมนวัตกรรมและสร้างมูลค่าเพิ่มเพื่อการค้า การค้าระหว่างประเทศ กระทรวงพาณิชย์ ณ
                          ตึกสันติไมตรี ทำเนียบรัฐบาล เมื่อวันศุกร์ที่ 24 สิงหาคม 2561</p>

                        <p>กรุงเทพฯ – นายกรัฐมนตรี พลเอกประยุทธ์ จันทร์โอชา
                          เป็นประธานในพิธีมอบรางวัลผู้ประกอบธุรกิจส่งออกดีเด่น (Prime Minister’s Export Award : PM
                          Award
                          2018) รับรองคุณภาพของผู้ประกอบการธุรกิจส่งออกไทย ให้เป็นที่ประจักษ์แก่สากล
                          ในปีนี้ได้ใช้แนวคิด
                          ‘Leading The Way’ สะท้อนถึงเส้นทางเดินแห่งความสำเร็จของนักธุรกิจไทย ซึ่งจัดโดย
                          สำนักส่งเสริมนวัตกรรมและสร้างมูลค่าเพิ่มเพื่อการค้า การค้าระหว่างประเทศ กระทรวงพาณิชย์ ณ
                          ตึกสันติไมตรี ทำเนียบรัฐบาล เมื่อวันศุกร์ที่ 24 สิงหาคม 2561</p>
                      </div>


                    </div>
                    <!-- VIDEO -->
                    <div class="news-box-video is-revealing js-lightbox js-gallery">
                      <div class="figure">
                        <div class="figure-wrap">
                          <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                        </div>
                        <div class="figure-overlay"></div>
                      </div>
                      <div class="news-box-video-play btn-play is-revealing js-gallery--item" data-poster="assets/images/news-event/video.jpg" data-type="youtube" data-url="https://www.youtube.com/watch?v=ZGkQ9-mZM1Q">
                        <a href="" class=" js-lightbox-link js-gallery--trigger"><?php include('inc/icon-play.php'); ?></a>
                      </div>
                    </div>
                    <!-- CONTENT -->
                    <div class="news-box-content is-revealing">
                      <div class="news-box-content-desc">
                        <p>กรุงเทพฯ – นายกรัฐมนตรี พลเอกประยุทธ์ จันทร์โอชา
                          เป็นประธานในพิธีมอบรางวัลผู้ประกอบธุรกิจส่งออกดีเด่น (Prime Minister’s Export Award : PM
                          Award
                          2018) รับรองคุณภาพของผู้ประกอบการธุรกิจส่งออกไทย ให้เป็นที่ประจักษ์แก่สากล
                          ในปีนี้ได้ใช้แนวคิด
                          ‘Leading The Way’ สะท้อนถึงเส้นทางเดินแห่งความสำเร็จของนักธุรกิจไทย ซึ่งจัดโดย
                          สำนักส่งเสริมนวัตกรรมและสร้างมูลค่าเพิ่มเพื่อการค้า การค้าระหว่างประเทศ กระทรวงพาณิชย์ ณ
                          ตึกสันติไมตรี ทำเนียบรัฐบาล เมื่อวันศุกร์ที่ 24 สิงหาคม 2561</p>

                        <p>กรุงเทพฯ – นายกรัฐมนตรี พลเอกประยุทธ์ จันทร์โอชา
                          เป็นประธานในพิธีมอบรางวัลผู้ประกอบธุรกิจส่งออกดีเด่น (Prime Minister’s Export Award : PM
                          Award
                          2018) รับรองคุณภาพของผู้ประกอบการธุรกิจส่งออกไทย ให้เป็นที่ประจักษ์แก่สากล
                          ในปีนี้ได้ใช้แนวคิด
                          ‘Leading The Way’ สะท้อนถึงเส้นทางเดินแห่งความสำเร็จของนักธุรกิจไทย ซึ่งจัดโดย
                          สำนักส่งเสริมนวัตกรรมและสร้างมูลค่าเพิ่มเพื่อการค้า การค้าระหว่างประเทศ กระทรวงพาณิชย์ ณ
                          ตึกสันติไมตรี ทำเนียบรัฐบาล เมื่อวันศุกร์ที่ 24 สิงหาคม 2561</p>
                      </div>
                    </div>
                    <!-- GALLERY -->
                    <div class="news-box-gallery is-revealing js-lightbox js-gallery">
                      <div class="news-gallery-lists">
                        <!-- GALLERY 1 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 2 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 3 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 4 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 5 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 6 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 7 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 8 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 9 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                          </a>
                        </div>
                        <!-- GALLERY 11 -->
                        <div class="news-gallery-list js-gallery--item" data-img="assets/images/news-event/video.jpg">
                          <a href="assets/images/news-event/video.jpg" class="news-gallery-box js-lightbox-link js-gallery--trigger">
                            <div class="figure">
                              <div class="figure-wrap">
                                <img data-src="assets/images/news-event/video.jpg" alt="" class="js-imageload">
                              </div>
                            </div>
                            <div class="news-gallery-number">
                              <span>19+</span>
                            </div>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('inc/cta.php'); ?>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <?php include('inc/modal-gallery.php'); ?>
  <script src="assets/scripts/main.js"></script>
</body>

</html>