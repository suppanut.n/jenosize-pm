<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title></title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <?php
    $bannerApi = 'api/contact.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner.php'); ?>
      <?php } ?>
      <!-- CONTACT -->
      <section class="section section-contact">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="contact">
                <div class="contact-row">
                  <div class="contact-col">
                    <!-- CONTACT HEADLINE -->
                    <div class="contact-headline is-revealing">
                      <div class="contact-headline-title">
                        <h2 class="h3 text-uppercase text-heading-bold">PRIME MINISTER’S <br>EXPORT AWARD</h2>
                      </div>
                    </div>
                    <!-- CONTACT ADDRESS -->
                    <div class="contact-address is-revealing">
                      <div class="contact-address-title">
                        <h3 class="h4 text-uppercase text-heading-bold">CONTACT ADDRESS</h3>
                      </div>
                      <div class="contact-address-detail">
                        <address>
                          563 Nonthaburi Rd., Bangkasor Nonthaburi 11000<br>
                          APPLYING INFORMATION<br>
                          02-507-8333, 02-507-8341, 02-507-8394<br>
                          EMAIL <a href="">thaiselect.ditp@gmail.com</a>
                        </address>
                      </div>
                    </div>
                  </div>
                  <div class="contact-col">
                    <!-- CONTACT FORM -->
                    <div class="contact-form is-revealing">
                      <!-- TITLE -->
                      <div class="contact-form-title">
                        <h3 class="h4 text-uppercase text-heading-bold">Contact Form</h3>
                      </div>
                      <!-- FORM -->
                      <div class="box-form">
                        <form action="" class="js-validate" novalidate>
                          <!-- FORM GROUP -->
                          <div class="form-group">
                            <!-- ROW -->
                            <div class="form-row">
                              <!-- COL -->
                              <div class="form-col">
                                <div class="form-container">
                                  <label for="" class="form-label">Name</label>
                                  <input type="text" class="form-input js-validate--input" placeholder="" required>
                                </div>
                              </div>
                              <!-- COL -->
                              <div class="form-col --two">
                                <div class="form-container">
                                  <label for="" class="form-label">Email</label>
                                  <input type="email" id="" class="form-input js-validate--input" placeholder="" required>
                                </div>
                              </div>
                              <!-- COL -->
                              <div class="form-col --two">
                                <div class="form-container">
                                  <label for="" class="form-label">Telephone</label>
                                  <input type="tel" id="" class="form-input js-validate--input" placeholder="" required>
                                </div>
                              </div>
                              <!-- COL -->
                              <div class="form-col">
                                <div class="form-container">
                                  <label for="" class="form-label">Subject</label>
                                  <input type="text" id="" class="form-input js-validate--input" placeholder="" value="" required>
                                </div>
                              </div>
                              <!-- COL -->
                              <div class="form-col">
                                <div class="form-container">
                                  <label for="" class="form-label">Detail</label>
                                  <textarea name="" id="" cols="30" rows="4" class="form-input js-validate--input" required></textarea>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- BUTTON -->
                          <div class="form-button-wrapper">
                            <button type="reset" class="form-button btn --blue">Reset</button>
                            <button type="submit" class="form-button btn --gradient">Submit</button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <!-- MAP -->
      <section class="section section-map is-revealing">
        <div class="map-wrapper">
          <div class="ggmap-wrapper">
            <div id="ggmap" class="ggmap" data-title="" data-lat="13.79789" data-lng="100.4321364" data-zoom="12"
              data-link="https://goo.gl/maps/pA1jKBsubG12"></div>
            <div id="zoom-in"></div>
            <div id="zoom-out"></div>
          </div>
        </div>
      </section>
      <?php include('inc/cta.php'); ?>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>