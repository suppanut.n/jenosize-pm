<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">

        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="">
            <!-- STEP HEADLINE -->
            <div class="step-headline-preview">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-preview-wrapper">
                      <div class="preview-logo">
                        <img data-src="assets/images/logos/logo-green.svg" alt="Prime Minister’s Export Award 2018"
                          class="js-imageload-self">
                      </div>
                      <div class="preview-title">
                        <div class="preview-title-award-logo">
                          <img src="assets/images/application/categories/best-green-innovation.svg" alt="Best Green Innovation">
                        </div>
                        <div class="preview-title-text">
                          <h3 class="h2">BEST GREEN INNOVATION</h3>
                        </div>
                      </div>
                      <div class="preview-page">4/6</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP CONTENT 4 -->
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 4</h3>
                        <p>การวัดการวิเคราะห์และการจัดการความรู้</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การจัดการสารสนเทศและองค์ความรู้ (สามารถระบุได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการบันทึกขั้นตอนการผลิตผลิตภัณฑ์</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการถ่ายทอดความเป็นมาของผลิตภัณฑ์ให้เป็นที่รู้จักต่อภายในองค์กรหรือชุมชน</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีกระบวนการสืบทอด/รักษา/ส่งเสริมการสร้างองค์ความรู้เกี่ยวกับผลิตภัณฑ์ต่อสมาชิกภายในองค์กรหรือชุมชน</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการเปิดให้เยี่ยมชมสถานที่ผลิตผลิตภัณฑ์แก่บุคคลทั่วไปทั้งคนไทยและคนต่างประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการแลกเปลี่ยนองค์ความรู้กับชุมชน ธุรกิจ
                              หรือหน่วยงานที่เกี่ยวข้อง</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- STEP CONTENT 5 -->
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 5</h3>
                        <p>บุคลากร</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>สวัสดิการพนักงาน นอกเหนือจากที่กฎหมายกำหนด (สามารถระบุได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีสวัสดิการที่มุ่งพัฒนาลูกจ้าง เช่น
                              การส่งเสริมการศึกษาทั้งในและนอกเวลาทำงาน
                              การอบรมความรู้เกี่ยวกับการทำงานทั้งในและนอกสถานที่ทำงาน หรือมุมอ่านหนังสือ เป็นต้น</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีสวัสดิการนันทนาการและสุขภาพอนามัย เช่น
                              การจัดทัศนศึกษา การจัดงานเลี้ยงสังสรรค์ พนักงานการให้ความรู้เรื่องสุขภาพอนามัย เป็นต้น</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">ได้รับการรับรองมาตรฐานแรงงานไทยขั้นพื้นฐาน (มรท.
                              8001-2553)</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- STEP CONTENT 6 -->
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 6</h3>
                        <p>การดำเนินงาน</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การใช้วัตถุดิบในการผลิตเพื่อก่อให้เกิดมูลค่าเพิ่ม</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-6-radio-1" class="form-radio" checked>
                            <label for="" class="form-label-radio">มีการใช้วัตถุดิบในประเทศ มากกว่าร้อยละ 70</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-6-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีการใช้วัตถุดิบในประเทศ มากกว่าร้อยละ 30</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-6-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีการใช้วัตถุดิบที่เป็นมิตรกับสิ่งแวดล้อมหรือมีการลดการใช้วัตถุดิบที่ไม่เป็นมิตรกับสิ่งแวดล้อม
                              หรือเป็นพิษต่อผู้บริโภค เช่นการลดใช้ แร่ใยหิน ตะกั่ว ปรอท
                              โลหะหนัก หรือสารที่เป็นพิษอื่นๆ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-6-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีโครงการพัฒนาวัตถุดิบที่ใช้ในการผลิตเพื่อให้เกิดการทดแทนอย่างยั่งยืน</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-6-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีการใช้วัตถุดิบที่นำมาสร้างมูลค่าเพิ่ม เช่น
                              ข้าวหอมมะลิไทย, ไหมไทย, ผ้าฝ้ายทอมือ หรือสิ่งที่บ่งบอกความเป็นเอกลักษณ์ของชาติ</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การพัฒนาด้านกระบวนการผลิต เพื่อเพิ่มประสิทธิภาพ (สามารถระบุได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการเพิ่มประสิทธิภาพปัจจัยการผลิตทางด้านเทคโนโลยีของเครื่องจักรและอุปกรณ์
                              และมีการปรับปรุงและพัฒนากระบวนการผลิตอย่างต่อเนื่อง</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการเพิ่มศักยภาพและพัฒนาบุคลากรสำหรับกระบวนการผลิต</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการเพิ่มประสิทธิภาพโดยการลดต้นทุนการผลิต</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การพัฒนาด้านการจัดการอาชีวอนามัยและความปลอดภัย (สามารถระบุได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการสวมใส่อุปกรณ์ป้องกันอันตรายส่วนบุคคล (PPE)
                              หรือมีมาตรการป้องกันอันตราย</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการอบรมการใช้งานอุปกรณ์
                              หรือเครื่องจักรในการทำงาน</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการติดตั้งอุปกรณ์ป้องกันที่เครื่องจักร
                              (Machine Guard)</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการพัฒนาด้านการจัดการอาชีวอนามัยและความปลอดภัย
                              เป็นไปตามกฎหมาย หรือมาตรฐานสากล</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BOTTOM -->
            <div class="step-preview-bottom">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-preview-bottom-wrapper">
                    <div class="form-button-step">
                      <a href="step-preview-2.php" class="form-button btn">Prev</a>
                      <a href="step-preview-4.php" class="form-button btn">Next</a>
                    </div>
                    <!-- STEP BUTTON -->
                    <div class="form-button-preview">
                      <a href="step-company.php" class="form-button btn --blue">Edit</a>
                      <a href="" download class="form-button btn --blue"><i><img src="assets/images/icons/icon-download.svg" alt=""></i>Download <span>(PDF)</span></a>
                      <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg"
                              alt=""></i>Submit</button>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>