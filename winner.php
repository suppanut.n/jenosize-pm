<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title></title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <?php
    $bannerApi = 'api/winner-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
      // if($value->banner_size == 'large') {
      //   $bannerSize = 'large';
      // } else {
      //   $bannerSize = '';
      // }
    ?>
      <?php include('inc/banner.php'); ?>
      <?php } ?>
      <!-- WINNER LISTS -->
      <section class="section section-winner">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="winner-lists">
                <?php
              $listApi = 'api/winner-lists.json'; // path to your JSON file
              $listData = file_get_contents($listApi); // put the contents of the file into a variable
              $listArray = json_decode($listData); // decode the JSON feed
              foreach ($listArray as $value) {
              ?>
                <!-- WINNER BOX -->
                <div class="winner-list is-revealing">
                  <a href="" download class="winner-box">
                    <div class="winner-box-wrapper">
                      <!-- THUMB -->
                      <div class="winner-box-thumb">
                        <div class="bg">
                          <div class="bg-container">
                            <img data-src="<?php echo $value->bg_url ?>" alt="" class="js-imageload">
                          </div>
                        </div>
                      </div>
                      <!-- TOP -->
                      <div class="winner-box-top">
                        <h3 class="h2">
                          <?php echo $value->year ?>
                        </h3>
                      </div>
                      <!-- BOTTOM -->
                      <div class="winner-box-bottom">
                        <div class="winner-box-download">
                          <h4 class="h4">Download</h4>
                        </div>
                        <div class="winner-box-file">
                          <p class="winner-box-file-type">File
                            <?php echo $value->file_type ?>
                          </p>
                          <p class="winner-box-file-size">Size
                            <?php echo $value->file_size ?>
                          </p>
                        </div>
                      </div>
                    </div>
                  </a>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('inc/cta.php'); ?>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>