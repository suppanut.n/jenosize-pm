<div class="application-list is-revealing">
    <div class="application-box <?php echo $active ?>">
        <div class="application-box-wrapper">
            <div class="mock"></div>
            <!-- ADD APPLICATION -->
            <a href="#" class="application-box-add js-modal--trigger" data-id="#application-modal">
                <div class="application-box-add-icon">
                    <span class="icon-svg">
                        <canvas width="100px" height="100px"></canvas>
                        <img src="assets/images/icons/icon-plus.svg" alt="">
                    </span>
                </div>
                <div class="application-box-add-title">
                    <p class="h3">เพิ่มใบสมัคร</p>
                </div>
            </a>
            <!-- DETAIL -->
            <div class="application-box-detail">
                <div class="application-box-detail-wrapper">
                    <div class="application-box-detail-award-logo">
                        <img src="<?php echo $value->award_logo ?>" alt="<?php echo $value->award_title ?>">
                    </div>
                    <div class="application-box-detail-inner">
                        <div class="application-box-detail-award-title">
                            <h3 class="h2">
                                <?php echo $value->award_title ?>
                            </h3>
                        </div>
                        <div class="application-box-detail-company-title">
                            <h4 class="p">ชื่อบริษัท :
                                <?php echo $value->company_title ?>
                            </h4>
                        </div>
                        <div class="application-box-detail-service-title">
                            <h4 class="p">ชื่อตราสินค้า/บริการ :
                                <?php echo $value->service_title ?>
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
            <div class="application-box-detail-edit">
                <div class="application-box-detail-status">
                    <div class="application-status --<?php echo $value->status_color ?>">
                        <div class="application-status-color"></div>
                        <p class="application-status-text">
                            <?php echo $value->status_text ?>
                            <?php if(!empty($value->status_percentage)) { ?>
                            <span class="application-status-percentage">
                                <?php echo $value->status_percentage ?></span>%
                            <?php } ?>
                        </p>
                    </div>
                    <div class="application-box-detail-download">
                        <a href="#" download>
                            <span class="icon-svg">
                                <canvas width="32px" height="31px"></canvas>
                                <svg width="32px" height="31px" viewBox="0 0 32 31" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                    xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <path d="M30.4233,30.667 L1.2763,30.667 C0.5713,30.667 0.0003,30.096 0.0003,29.391 L0.0003,1.276 C0.0003,0.571 0.5713,0 1.2763,0 L30.4233,0 C31.1283,0 31.6993,0.571 31.6993,1.276 L31.6993,29.391 C31.6993,30.096 31.1283,30.667 30.4233,30.667"
                                        id="Fill-1" fill="#4F677D"></path>
                                    <polygon fill="#FFFFFF" points="21.2924 15.0498 21.2924 12.0038 15.8504 12.0038 10.4074 12.0038 10.4074 15.0498 5.4464 15.0498 15.8504 25.5428 26.2524 15.0498"></polygon>
                                    <polygon fill="#FFFFFF" points="10.381 10.8997 21.292 10.8997 21.292 9.7997 10.381 9.7997"></polygon>
                                    <polygon fill="#FFFFFF" points="10.381 8.6957 21.292 8.6957 21.292 7.5957 10.381 7.5957"></polygon>
                                </svg>
                            </span>
                            <p>Download<span>(PDF)</span></p>
                        </a>
                    </div>
                </div>
                <a href="step-company.php" class="btn --gradient">( <?php echo $value->button_label ?> )</a>
            </div>
        </div>
    </div>
</div>