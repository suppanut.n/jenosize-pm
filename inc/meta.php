<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<base href="">
<meta name="description" content="">

<meta property="og:type" content="website">
<meta property="og:url" content="en">
<meta property="og:site_name" content="">
<meta property="og:image" content="">
<meta property="og:description" content="">


<meta property="twitter:card" content="summary">
<meta property="twitter:url" content="en">
<meta property="twitter:image" content="">
<meta property="twitter:description" content="">

<!-- DNS Prefetching -->
<link rel="dns-prefetch" href="//ajax.googleapis.com" />
<link rel="dns-prefetch" href="//fonts.googleapis.com" />

<!-- Viewport -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, viewport-fit=cover">
<meta name="HandheldFriendly" content="True">

<!-- Appearance -->
<link rel="apple-touch-icon" href="">
<link rel="icon" type="image/png" sizes="32x32" href="">
<link rel="icon" type="image/png" sizes="16x16" href="">
<link rel="icon" href="">
<link rel="manifest" href="">
<link rel="mask-icon" href="">
<meta name="msapplication-TileColor" content="">
<meta name="theme-color" content="">

<link rel="stylesheet" href="assets/styles/main.css">

<link rel="shortcut icon" href="assets/favicons/favicon.ico" type="image/x-icon" />
<link rel="apple-touch-icon" sizes="57x57" href="assets/favicons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="assets/favicons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="assets/favicons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="assets/favicons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="assets/favicons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="assets/favicons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="assets/favicons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="assets/favicons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="assets/favicons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="assets/favicons/favicon-16x16.png" sizes="16x16">
<link rel="icon" type="image/png" href="assets/favicons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/favicons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="assets/favicons/android-chrome-192x192.png" sizes="192x192">
<meta name="msapplication-square70x70logo" content="assets/favicons/smalltile.png" />
<meta name="msapplication-square150x150logo" content="assets/favicons/mediumtile.png" />
<meta name="msapplication-wide310x150logo" content="assets/favicons/widetile.png" />
<meta name="msapplication-square310x310logo" content="assets/favicons/largetile.png" />



