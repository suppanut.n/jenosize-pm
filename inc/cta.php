    <!-- CTA -->
    <section class="section section-cta">
      <div class="fixed-layout">
        <div class="section-outer">
          <div class="section-inner">
            <div class="cta">
              <div class="cta-title is-revealing">
                <h4 class="h4 text-uppercase text-heading-bold">Prime Minister’s Export Award</h4>
              </div>
              <div class="cta-desc is-revealing">
                <p>สัญลักษณ์ที่แสดงตัวตนบ่งบอกผลิตภัณฑ์คุณพร้อมรองรับสำหรับชาวไทยทั่วโลก</p>
              </div>
              <div class="cta-button is-revealing">
                <a href="register.php" class="btn --gradient">REGISTER</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>