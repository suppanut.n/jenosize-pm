<span class="icon-svg">
    <canvas width="16px" height="16px"></canvas>
    <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <polygon id="Fill-1" fill="#ED1C24" points="2.1213 -0.0001 0.0003 2.1209 13.2083 15.3299 15.3293 13.2079"></polygon>
            <polygon id="Fill-2" fill="#ED1C24" points="13.2084 -0.0001 0.0004 13.2079 2.1214 15.3299 15.3294 2.1209"></polygon>
        </g>
    </svg>
</span>