<!-- MODAL VIDEO -->
<div class="modal js-modal" id="gallery-modal">
    <div class="modal-wrapper">
        <div class="modal-background js-modal--close"></div>
        <div class="modal-outer">
            <div class="modal-inner">
                <div class="modal-container">
                    <div class="modal-content">
                        <div class="modal-content-item">
                            <!-- CONTENT -->
                            <div class="gallery-modal">
                                <div class="modal-slider-container js-gallery--slider">
                                    
                                </div>
                            </div>
                            <!-- END CONTENT -->
                        </div>
                        <!-- CLOSE -->
                        <div class="modal-button-close">
                            <a href="#" class="js-modal--close">
                                <div class="modal-button-close-bg"></div>
                                <span>Close</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>