<!-- MODAL -->
<div class="modal js-modal" id="application-modal">
  <div class="modal-wrapper">
    <div class="modal-background js-modal--close"></div>
    <div class="modal-outer">
      <div class="modal-inner">
        <div class="modal-container">
          <div class="modal-content">
            <div class="modal-content-item">
              <!-- APPLICATION MODAL -->
              <div class="application-modal">
                <!-- HEADLINE -->
                <div class="application-modal-headline">
                  <div class="application-modal-headline-title">
                    <h2 class="h2 text-heading text-uppercase">Application</h2>
                  </div>
                  <div class="application-modal-headline-desc">
                    <p>กรุณาเลือกหมวดหมู่สินค้าหรือบริการที่ตรงกับบริษัทท่าน</p>
                  </div>
                </div>
                <div class="application-categories-items">
                  <?php
                    $popularApi = 'api/application-categories.json'; // path to your JSON file
                    $popularData = file_get_contents($popularApi); // put the contents of the file into a variable
                    $popularArray = json_decode($popularData); // decode the JSON feed
                    foreach ($popularArray as $value) {
                  ?>
                  <div class="application-categories-item">
                    <div class="application-box">
                      <div class="application-box-wrapper">
                        <div class="mock"></div>
                        <!-- CATEGORIES -->
                        <div class="application-categories-block">
                          <div class="application-categories-wrapper">
                            <div class="application-categories-award-logo">
                              <img src="<?php echo $value->award_logo ?>" alt="<?php echo $value->award_title ?>">
                            </div>
                            <div class="application-categories-award-title">
                              <h3 class="h4 text-uppercase">
                                <?php echo $value->award_title ?>
                              </h3>
                            </div>
                          </div>
                        </div>
                        <a href="my-application-data.php" class="application-categories-block">
                          <div class="application-categories-wrapper">
                            <div class="application-categories-award-logo">
                              <img src="<?php echo $value->award_logo ?>" alt="<?php echo $value->award_title ?>">
                            </div>
                            <div class="application-categories-award-title">
                              <h3 class="h4 text-uppercase">
                                <?php echo $value->award_title ?>
                              </h3>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <!-- END CONTENT -->
            </div>
            <!-- CLOSE -->
            <div class="modal-button-close">
              <a href="#" class="js-modal--close">
                <div class="modal-button-close-bg"></div>
                <span>Close</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>