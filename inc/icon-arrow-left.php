<span class="icon-svg">
    <canvas width="36px" height="12px"></canvas>
    <svg width="36px" height="12px" viewBox="0 0 36 12" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Group-14" fill="currentColor" fill-rule="nonzero">
                <polygon id="Triangle" transform="translate(5.809017, 5.809017) scale(-1, 1) translate(-5.809017, -5.809017) "
                    points="0 11.618034 0 0 11.618034 5.80901699"></polygon>
                <polygon id="Line" transform="translate(19.905775, 6.000000) scale(-1, 1) translate(-19.905775, -6.000000) "
                    points="4 7 4 5 35.8115507 5 35.8115507 7"></polygon>
            </g>
        </g>
    </svg>
</span>