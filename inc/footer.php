<footer class="site-footer">
    <div class="fixed-layout">
        <div class="section-outer">
            <!-- FOOTER LOGO -->
            <div class="footer-logo">
                <a href="index.php" class="site-footer-logo">
                    <img data-src="assets/images/logos/logo.svg" alt="Prime Minister’s Export Award 2018" class="js-imageload-self">
                </a>
            </div>
            <div class="footer-row">
                <div class="footer-col">
                    <!-- FOOTER ADDRESS -->
                    <div class="footer-address">
                        <div class="footer-title">
                            <h4 class="h5">กรมส่งเสริมการค้าระหว่างประเทศ (บางกระสอ)</h4>
                        </div>
                        <div class="footer-content">
                            <p>563 ถนนนนทบุรี ตำบลบางกระสอ อำเภอเมือง จังหวัดนนทบุรี 11000</p>
                        </div>
                    </div>
                    <!-- FOOTER NEWSLETTER -->
                    <div class="footer-newsletter">
                        <div class="footer-title">
                            <h4 class="h5">สมัครรับข่าวสารจาก PM Award</h4>
                        </div>
                        <form action="" class="newsletter-form js-validate" novalidate>
                            <div class="newsletter-form__border js-validate--input">
                                <label for="">Email</label>
                                <input type="email" name="" id="" placeholder="Your Email" required>
                                <!-- <button>Subscribe</button> -->
                                <button type="submit"><svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1"><path fill="currentColor" fill-rule="evenodd" stroke-width="0.1" d="M7.903 0L16 8l-8.097 8L6.8 14.92l6.23-6.155H0v-1.53h13.03L6.8 1.08z"></path></svg></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="footer-col">
                    <!-- FOOTER MENU -->
                    <div class="footer-menu">
                        <div class="footer-content">
                            <ul>
                                <li><a href="index.php">หน้าหลัก</a></li>
                                <li><a href="about-us.php">เกี่ยวกับ PM</a></li>
                                <li><a href="register.php">สมัครเข้าประกวด</a></li>
                                <li><a href="winner.php">รายชื่อผู้ชนะ</a></li>
                                <li><a href="news-event.php">ข่าวและกิจกรรม</a></li>
                                <li><a href="contact.php">ติดต่อเรา</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- FOOTER COPYRIGHT -->
                    <div class="footer-copyright">
                        <p>Copyright &copy; 2015-2017 Department of International Trade Promotion, Ministry of
                            Commerce,
                            Royal Thai
                            Government. All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>