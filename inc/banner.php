<!-- BANNER -->
<section class="section section-banner" data-size="<?php echo $value->banner_size ?>">
    <div class="banner">
        <!-- BG -->
        <div class="banner-bg bg">
            <div class="bg-container">
                <img data-src="<?php echo $value->bg_url ?>" alt="" class="js-imageload">
            </div>
        </div>
        <!-- TEXT -->
        <div class="banner-content">
            <div class="fixed-layout">
                <div class="section-outer">
                    <div class="section-inner">
                        <div class="banner-headline">
                            <div class="banner-site-title is-revealing">
                                <h2 class="h1 text-heading-bold"><?php echo $value->site_title ?></h2>
                            </div>
                            <div class="banner-title is-revealing">
                                <h1 class="h1"><?php echo $value->title ?></h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>