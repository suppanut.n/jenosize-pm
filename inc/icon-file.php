<span class="icon-svg">
    <canvas width="23px" height="30px"></canvas>
    <svg width="23px" height="30px" viewBox="0 0 23 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <path d="M1,28.374 L21.453,28.374 L21.453,1 L7.109,1 L1,7.109 L1,28.374 Z M21.953,29.374 L0.5,29.374 C0.224,29.374 0,29.15 0,28.874 L0,6.902 C0,6.769 0.053,6.642 0.146,6.548 L6.548,0.147 C6.642,0.053 6.769,0 6.901,0 L21.953,0 C22.229,0 22.453,0.224 22.453,0.5 L22.453,28.874 C22.453,29.15 22.229,29.374 21.953,29.374 Z"
                id="Fill-1" fill="#FFFFFF"></path>
        </g>
    </svg>
</span>