<header class="site-header">
    <div class="fixed-layout">
        <div class="section-outer">
            <div class="site-header-wrapper">
                <div class="site-header-left">
                    <a href="index.php" class="site-logo">
                        <img data-src="assets/images/logos/logo-white.svg" alt="Prime Minister’s Export Award 2018" class="js-imageload-self">
                    </a>
                </div>
                <div class="site-header-right">
                    <div class="site-nav-wrapper">
                        <div class="site-nav">
                            <ul>
                                <li><a href="index.php">หน้าหลัก</a></li>
                                <li><a href="about-us.php">เกี่ยวกับ PM</a></li>
                                <li><a href="winner.php">รายชื่อผู้ชนะ</a></li>
                                <li><a href="news-event.php">ข่าวและกิจกรรม</a></li>
                                <li><a href="contact.php">ติดต่อเรา</a></li>
                            </ul>
                        </div>
                        <div class="login-button">
                            <!-- if user is logged in -->
                            <div class="login-button-wrapper">
                                <div class="login-button-trigger">
                                    <i><img src="assets/images/icons/icon-login.svg" alt=""></i>
                                    <span>Kanmanut Iampinit</span>
                                    <button class="subnav-toggle"></button>
                                </div>
                                <div class="login-dropdown">
                                    <ul>
                                        <li><a href="my-application.php">My Application</a></li>
                                        <li><a href="edit-profile.php">Edit Profile</a></li>
                                        <li><a href="edit-password.php">Edit Password</a></li>
                                        <li><a href="login.php">Log out</a></li>
                                    </ul>
                                </div>
                            </div>
                            <!-- if not logged in -->
                            <div class="login-button-wrapper is--active">
                                <a href="login.php" class="login-button-trigger">
                                    <i><img src="assets/images/icons/icon-login.svg" alt=""></i><span>Login</span>
                                </a>
                            </div>
                        </div>
                        <div class="register-button">
                            <a href="register.php" class="btn">Register</a>
                        </div>
                    </div>
                    <div class="menu__toggle">
                        <a href="#" title="">
                            <div class="menu__toggle__inner">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>