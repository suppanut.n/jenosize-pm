<svg width="31px" height="18px" viewBox="0 0 31 18" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
        <g transform="translate(1.000000, 1.000000)" stroke="#FFFFFF" stroke-width="2.674">
            <polyline id="Stroke-1" points="28.359 -2.48689958e-14 14.179 14.18 1.77635684e-14 -2.48689958e-14"></polyline>
        </g>
    </g>
</svg>