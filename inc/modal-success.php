<!-- MODAL -->
<div class="modal succes-modal js-modal" id="success-modal">
    <div class="modal-wrapper">
        <div class="modal-background js-modal--close"></div>
        <div class="modal-outer">
            <div class="modal-inner">
                <div class="modal-container">
                    <div class="modal-content">
                        <div class="modal-content-item">
                            <!-- SUCCESS MODAL -->
                            <div class="succes-modal-wrapper">
                                <div class="succes-modal-icon">
                                    <img src="assets/images/icons/icon-success.svg" alt="">
                                </div>
                                <div class="succes-modal-content">
                                    <h3 class="h3">เราได้รับ APPLICATION ของคุณ เข้าระบบเรียบร้อยแล้ว</h3>
                                </div>
                                <div class="succes-modal-button">
                                    <a href="step-company.php" class="form-button btn --blue">Edit</a>
                                    <a href="" download class="form-button btn --gradient"><i><img src="assets/images/icons/icon-download.svg"
                                                alt=""></i>Download <span>(PDF)</span></a>
                                </div>
                            </div>
                            <!-- END CONTENT -->
                        </div>
                        <!-- CLOSE -->
                        <div class="modal-button-close">
                            <a href="#" class="js-modal--close">
                                <div class="modal-button-close-bg"></div>
                                <span>Close</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>