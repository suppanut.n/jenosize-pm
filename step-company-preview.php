<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="">
            <!-- STEP HEADLINE -->
            <div class="step-headline-preview">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-preview-wrapper">
                      <div class="preview-logo">
                        <img data-src="assets/images/logos/logo-green.svg" alt="Prime Minister’s Export Award 2018"
                          class="js-imageload-self">
                      </div>
                      <div class="preview-title">
                        <div class="preview-title-award-logo">
                          <img src="assets/images/application/categories/best-green-innovation.svg" alt="Best Green Innovation">
                        </div>
                        <div class="preview-title-text">
                          <h3 class="h2">BEST GREEN INNOVATION</h3>
                        </div>
                      </div>
                      <div class="preview-page">1/6</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP -->
                    <div class="step">
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">ชื่อบริษัท</label>
                        <div class="form-input-row">
                          <div class="form-input-col --full">
                            <input type="text" id="" class="form-input" placeholder="(ภาษาไทย)" value="เจโนไซส์">
                          </div>
                          <div class="form-input-col --full">
                            <input type="text" id="" class="form-input" placeholder="(ภาษาอังกฤษ)" value="Jenosize">
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">หมายเลขทะเบียนนิติบุคคล</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <input type="text" id="" class="form-input" placeholder="(ภาษาไทย)" value="1103300054530">
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ที่อยู่บริษัท</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">เลขที่:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="55/93">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">หมู่ที่:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="5">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตรอก/ซอย:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="4">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ถนน:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="แจ้งวัฒนะ">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">แขวง/ตำบล:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="ปากเกร็ด">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">เขต/อำเภอ:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="ปากเกร็ด">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">จังหวัด:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="นนทบุรี">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">รหัสไปรษณีย์:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="11120">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรสาร:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="025833876">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="025833877">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="info@jenosize.com">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">Website:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="www.jenosize.com">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">เครื่องหมายการค้า</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- FILE -->
                            <div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">ประเภทสินค้า</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- FILE -->
                            <div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>

                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>ขนาดของกิจการ</h4>
                        </div>
                        <div class="form-lists --inline --large">
                          <div class="form-list">
                            <input type="radio" id="" name="step-company-radio-1" class="form-radio" checked>
                            <label for="" class="form-label-radio">ขนาดเล็ก</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-company-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ขนาดกลาง</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-company-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ขนาดใหญ่</label>
                          </div>
                        </div>
                      </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <label for="" class="form-label">มูลค่าสินทรัพย์ถาวรสุทธิ (ไม่รวมที่ดิน)</label>
                          <div class="form-input-row">
                            <div class="form-input-col">
                              <input type="text" id="" class="form-input" placeholder="(บาท)" value="10,000,000 บาท">
                            </div>
                          </div>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <label for="" class="form-label">จำนวนการจ้างงาน</label>
                          <div class="form-input-row">
                            <div class="form-input-col">
                              <input type="text" id="" class="form-input" placeholder="(คน)" value="100 คน">
                            </div>
                          </div>
                        </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>เจ้าหน้าที่ประสานงาน</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ชื่อ</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="พัสวีอาภา">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตำแหน่ง</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="พนักงานทั่วไป">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="my@jenosize.com">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="025833876">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ชื่อ</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="ชลธิดา">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตำแหน่ง</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="พนักงานทั่วไป">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="0824415365">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="cc@jenosize.com">
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ผู้มีอำนาจลงนามของบริษัท</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ชื่อ</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="ณัฐเศรษฐ์">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตำแหน่ง</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="CEO">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="0824415365">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="max@jenosize.com">
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BOTTOM -->
            <div class="step-preview-bottom">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-preview-bottom-wrapper">
                      <div class="form-button-step">
                        <a href="step-company-preview.php" class="form-button btn">Prev</a>
                        <a href="step-preview-1.php" class="form-button btn">Next</a>
                      </div>
                      <!-- STEP BUTTON -->
                      <div class="form-button-preview">
                        <a href="step-company.php" class="form-button btn --blue">Edit</a>
                        <a href="" download class="form-button btn --blue"><i><img src="assets/images/icons/icon-download.svg"
                              alt=""></i>Download <span>(PDF)</span></a>
                        <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg"
                              alt=""></i>Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>