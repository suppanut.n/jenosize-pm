<!doctype html>
<html lang="en">

<head>
    <?php include('inc/meta.php'); ?>
    <title>Edit Profile</title>
</head>

<body class="has-animations">
    <?php include('inc/loader.php'); ?>
    <div class="site-global">
        <?php include('inc/header-2.php'); ?>
        <main class="site-main">
            <!-- REGISTER -->
            <section class="section section-register section-form">
                <div class="fixed-layout">
                    <div class="section-outer">
                        <div class="section-inner">
                            <div class="form-wrapper edit-profile-form">
                                <!-- HEADLINE -->
                                <div class="form-wrapper-headline is-revealing">
                                    <h1 class="h3 text-uppercase text-heading-bold">Edit Profile</h1>
                                </div>
                                <!-- REGISTER FORM -->
                                <div class="box-form is-revealing">
                                    <form class="js-validate" autocomplete="off" enctype="multipart/form-data"
                                        novalidate>
                                        <!-- FORM GROUP -->
                                        <div class="form-group">
                                            <!-- ROW -->
                                            <div class="form-row">
                                                <!-- COL -->
                                                <div class="form-col --two">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">Name</label>
                                                        <input type="text" class="form-input js-validate--input"
                                                            placeholder="" value="Kanmanut" required>
                                                    </div>
                                                </div>
                                                <!-- COL -->
                                                <div class="form-col --two">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">Last name</label>
                                                        <input type="text" class="form-input js-validate--input"
                                                            placeholder="" value="Iampinit" required>
                                                    </div>
                                                </div>
                                                <!-- COL -->
                                                <div class="form-col --two">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">Email</label>
                                                        <input type="email" class="form-input js-validate--input"
                                                            placeholder="" value="gun@jenosize.com" required>
                                                    </div>
                                                </div>
                                                <!-- COL -->
                                                <div class="form-col --two">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">Telephone</label>
                                                        <input type="tel" class="form-input js-validate--input"
                                                            placeholder="" value="0808092809" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- BUTTON -->
                                        <div class="form-button-wrapper">
                                            <button type="submit" class="form-button btn --gradient">Save</button>
                                            <button type="reset" class="form-button btn --blue">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include('inc/footer.php'); ?>
    </div>

    <script src="assets/scripts/main.js"></script>
</body>

</html>