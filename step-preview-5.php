<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="">
            <!-- STEP HEADLINE -->
            <div class="step-headline-preview">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-preview-wrapper">
                      <div class="preview-logo">
                        <img data-src="assets/images/logos/logo-green.svg" alt="Prime Minister’s Export Award 2018"
                          class="js-imageload-self">
                      </div>
                      <div class="preview-title">
                        <div class="preview-title-award-logo">
                          <img src="assets/images/application/categories/best-green-innovation.svg" alt="Best Green Innovation">
                        </div>
                        <div class="preview-title-text">
                          <h3 class="h2">BEST GREEN INNOVATION</h3>
                        </div>
                      </div>
                      <div class="preview-page">6/6</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP CONTENT 7 -->
                    <div class="step">

                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มาตรฐานคุณภาพสินค้าที่ได้รับ (เลือกได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มาตรฐานคุณภาพผลิตภัณฑ์ขั้นพื้นฐานในประเทศที่ออกโดยหน่วยงานของเอกชน
                              เช่น มาตรฐานของมูลนิธิสิ่งแวดล้อมไทย</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">ได้รับมาตรฐานคุณภาพผลิตภัณฑ์ขั้นพื้นฐานในประเทศที่ออกโดยหน่วยงานของรัฐ
                              เช่น GMP, อย.อย.</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มาตรฐานของสมาคม/องค์กร/หน่วยธุรกิจในประเทศต่างๆ
                              เช่น มาตรฐานของสมาคมค้าปลีก</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มาตรฐานระดับสากล เช่น ISO, HACCP เป็นต้น</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน พร้อม หน้าข้อที่มีหลักฐาน)
                          สำเนาใบรับรองมาตรฐานต่าง ๆ ที่ได้รับ</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- FILE -->
                            <div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line text-center">
                          <h4>ขอรับรองว่าทั้งหมดเป็นความจริง</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ลายมือชื่อ (ผู้มีอำนาจลงนามของบริษัท)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)" value="พัสวีอาภา เลิศพัฒนพร">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --six">
                            <label for="" class="form-label">วัน</label>
                            <div class="form-input-row">
                              <div class="form-input-col">  
                                <div class="select-wrapper">
                                  <select class="js-selectric">
                                  <option value="">(วว)</option>
                                  <option value="">01</option>
                                  <option value="">02</option>
                                  <option value="">03</option>
                                  <option value="">04</option>
                                  <option value="">05</option>
                                  <option value="">06</option>
                                  <option value="">07</option>
                                  <option value="">08</option>
                                  <option value="">09</option>
                                  <option value="">10</option>
                                  <option value="">11</option>
                                  <option value="">12</option>
                                  <option value="">13</option>
                                  <option value="">14</option>
                                  <option value="">15</option>
                                  <option value="">16</option>
                                  <option value="">17</option>
                                  <option value="">18</option>
                                  <option value="">19</option>
                                  <option value="">20</option>
                                  <option value="">21</option>
                                  <option value="">22</option>
                                  <option value="">23</option>
                                  <option value="">24</option>
                                  <option value="">25</option>
                                  <option value="">26</option>
                                  <option value="">27</option>
                                  <option value="">28</option>
                                  <option value="">29</option>
                                  <option value="">30</option>
                                  <option value="">31</option>
                                  </select>
                                  <?php include('inc/select-arrow.php'); ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <!-- COL -->
                          <div class="form-col --six">
                            <label for="" class="form-label">เดือน</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <div class="select-wrapper">
                                  <select class="js-selectric">
                                    <option value="">(ดด)</option>
                                    <option value="มกราคม">มกราคม</option>
                                    <option value="กุมภาพันธ์">กุมภาพันธ์</option>
                                    <option value="มีนาคม">มีนาคม</option>
                                    <option value="เมษายน">เมษายน</option>
                                    <option value="พฤษภาคม">พฤษภาคม</option>
                                    <option value="มิถุนายน">มิถุนายน</option>
                                    <option value="กรกฎาคม">กรกฎาคม</option>
                                    <option value="สิงหาคม">สิงหาคม</option>
                                    <option value="กันยายน">กันยายน</option>
                                    <option value="ตุลาคม">ตุลาคม</option>
                                    <option value="พฤศจิกายน">พฤศจิกายน</option>
                                    <option value="ธันวาคม">ธันวาคม</option>
                                  </select>
                                  <?php include('inc/select-arrow.php'); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --six">
                            <label for="" class="form-label">ปี</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <div class="select-wrapper">
                                  <select class="js-selectric">
                                    <option value="">(ปป)</option>
                                    <option value="2561">2561</option>
                                    <option value="2562">2562</option>
                                    <option value="2563">2563</option>
                                    <option value="2564">2564</option>
                                  </select>
                                  <?php include('inc/select-arrow.php'); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BOTTOM -->
            <div class="step-preview-bottom">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-preview-bottom-wrapper">
                      <div class="form-button-step">
                        <a href="step-preview-4.php" class="form-button btn">Prev</a>
                        <a href="step-preview-5.php" class="form-button btn">Next</a>
                      </div>
                      <!-- STEP BUTTON -->
                      <div class="form-button-preview">
                        <a href="step-company.php" class="form-button btn --blue">Edit</a>
                        <a href="" download class="form-button btn --blue"><i><img src="assets/images/icons/icon-download.svg"
                              alt=""></i>Download <span>(PDF)</span></a>
                              <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg"
                              alt=""></i>Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>