<!doctype html>
<html lang="en">

<head>
    <?php include('inc/meta.php'); ?>
    <title>Edit Password</title>
</head>

<body class="has-animations">
    <?php include('inc/loader.php'); ?>
    <div class="site-global">
        <?php include('inc/header-2.php'); ?>
        <main class="site-main">
            <!-- LOGIN -->
            <section class="section section-login section-form">
                <div class="fixed-layout">
                    <div class="section-outer">
                        <div class="section-inner">
                            <div class="form-wrapper login-form edit-password-form">
                                <!-- HEADLINE -->
                                <div class="form-wrapper-headline is-revealing">
                                    <h1 class="h3 text-uppercase text-heading-bold">Edit Password</h1>
                                </div>
                                <!-- LOGIN FORM -->
                                <div class="box-form is-revealing">
                                    <form action="" class="js-validate" novalidate>
                                        <!-- FORM GROUP -->
                                        <div class="form-group">
                                            <!-- ROW -->
                                            <div class="form-row">
                                                <!-- COL -->
                                                <div class="form-col">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">Current Password</label>
                                                        <input id="register-password" type="password" class="form-input js-validate--input"
                                                            placeholder="" value="" required>
                                                    </div>
                                                </div>
                                                <!-- COL -->
                                                <div class="form-col">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">New Password</label>
                                                        <input id="register-password" type="password" class="form-input js-validate--input"
                                                            placeholder="" value="" required>
                                                    </div>
                                                </div>
                                                <!-- COL -->
                                                <div class="form-col">
                                                    <div class="form-container">
                                                        <label for="" class="form-label">Confirm Password</label>
                                                        <input type="password" class="form-input js-validate--input"
                                                            placeholder="" value="" data-confirm="#register-password"
                                                            required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- BUTTON -->
                                        <div class="form-button-wrapper">
                                            <button type="submit" class="form-button btn --gradient">Save</button>
                                            <button type="reset" class="form-button btn --blue">Cancel</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>
        <?php include('inc/footer.php'); ?>
    </div>

    <script src="assets/scripts/main.js"></script>
</body>

</html>