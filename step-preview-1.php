<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="">
            <!-- STEP HEADLINE -->
            <div class="step-headline-preview">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-preview-wrapper">
                      <div class="preview-logo">
                        <img data-src="assets/images/logos/logo-green.svg" alt="Prime Minister’s Export Award 2018"
                          class="js-imageload-self">
                      </div>
                      <div class="preview-title">
                        <div class="preview-title-award-logo">
                          <img src="assets/images/application/categories/best-green-innovation.svg" alt="Best Green Innovation">
                        </div>
                        <div class="preview-title-text">
                          <h3 class="h2">BEST GREEN INNOVATION</h3>
                        </div>
                      </div>
                      <div class="preview-page">2/6</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP CONTENT 1 -->
                    <div class="step">
                      <!-- FORM HEADLINE -->
                      <div class="form-h">
                        <h3>หมวดที่ 1</h3>
                        <p>การบริหารองค์กร</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>ธรรมาภิบาลขององค์กร</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-1" class="form-radio" checked>
                            <label for="" class="form-label-radio">มีหลักความโปร่งใส เช่น
                              มีการจัดทำระบบตรวจสอบภายในองค์กร</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีหลักนิติธรรม เช่น มีการปฏิบัติตามกฎหมาย เช่น
                              การจัดส่งงบการเงิน การชำระภาษี </label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีหลักคุณธรรม เช่น มีการฝึกอบรมพนักงาน
                              เพื่อให้ปฏิบัติหน้าที่ด้วยความซื่อสัตย์และมีประสิทธิภาพ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีหลักความคุ้มค่า เช่น
                              มีการรณรงค์ให้พนักงานใช้ทรัพยากร เช่น น้ำ ไฟ กระดาษ
                              อย่างประหยัดและเกิดประโยชน์สูงสุดแก่ส่วนรวม </label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีหลักการมีส่วนร่วม เช่น
                              มีการส่งเสริมให้พนักงานร่วมกันเสนอความคิดเห็น ข้อแนะนำต่างๆ
                              เพื่อนำไปสู่การปฏิบัติหน้าที่ได้อย่างให้สัมฤทธิ์ผลและเป็นรูปธรรม</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีหลักความรับผิดชอบ เช่น
                              มีการกำหนดผังหน้าที่ความรับผิดชอบ กฎระเบียบองค์กรที่ชัดเจน</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีความรับผิดชอบต่อสังคมและสิ่งแวดล้อมอย่างยั่งยืน
                            (ตรวจเอกสารและเลือกให้คะแนนเพียงข้อเดียว)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-2" class="form-radio" checked>
                            <label for="" class="form-label-radio">มีแผนการดำเนินงานด้านสังคมและสิ่งแวดล้อม <strong>เข้าร่วมกิจกรรม</strong>หรือโครงการด้านสังคมและ
                              สิ่งแวดล้อมที่หน่วยงานอื่นเป็นผู้จัดอย่างน้อยต่อเนื่อง <strong>1 ปีขึ้นไป</strong>
                              และเป็นไปตามแผนที่กำหนด</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มีแผนการดำเนินงานด้านสังคมและสิ่งแวดล้อม <strong>ดำเนินกิจกรรม</strong>หรือโครงการด้านสังคมและสิ่งแวดล้อมที่เป็นเจ้าของโครงการอย่างน้อยต่อเนื่อง
                              <strong>1 ปีขึ้นไป</strong> และเป็นไปตามแผนที่กำหนด</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มีแผนการดำเนินงานด้านสังคมและสิ่งแวดล้อม <strong>ดำเนินกิจกรรม</strong>หรือโครงการด้านสังคมและสิ่งแวดล้อมที่เป็นเจ้าของโครงการอย่างน้อยต่อเนื่อง
                              <strong>2 ปีขึ้นไป</strong> และเป็นไปตามแผนที่กำหนด</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-1-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มีแผนการดำเนินงานด้านสังคมและสิ่งแวดล้อม <strong>ดำเนินกิจกรรม</strong>หรือโครงการด้านสังคมและสิ่งแวดล้อมที่เป็นเจ้าของโครงการอย่างน้อยต่อเนื่อง
                              <strong>3 ปีขึ้นไป</strong> และเป็นไปตามแผนที่กำหนด</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- STEP CONTENT 2 -->
                    <div class="step">
                      <!-- FORM HEADLINE -->
                      <div class="form-h">
                        <h3>หมวดที่ 2</h3>
                        <p>การวางแผนเชิงกลยุทธ์</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>แนวคิดในการพัฒนารูปแบบผลิตภัณฑ์ (ครอบคลุมบรรจุภัณฑ์) ในรอบ 1 - 2 ปี ที่ผ่านมา</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio" checked>
                            <label for="" class="form-label-radio">มีหน่วยงาน R&D
                              ทั้งในและนอกสถานที่เพื่อวิจัยและพัฒนาผลิตภัณฑ์/
                              มีการใช้งานวิจัยของนักวิจัยในประเทศหรือต่างประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีการพัฒนารูปแบบผลิตภัณฑ์เพื่อมีคุณภาพและมาตรฐานระดับสากล</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีการพัฒนารูปแบบผลิตภัณฑ์เพื่อตอบสนองตลาดหรือลูกค้าต่างประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มีการพัฒนารูปแบบผลิตภัณฑ์เพื่อสิ่งแวดล้อม</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>กลยุทธ์ในการสร้างตราสินค้า</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio" checked>
                            <label for="" class="form-label-radio">มีกลยุทธ์การสร้างเอกลักษณ์ของตราสินค้า (Brand
                              Identity) การนำเสนอความเป็นตัวตนที่ชัดเจน
                              และโดดเด่นของสินค้าเพื่อสร้างความแตกต่างจากคู่แข่ง เน้นย้ำให้ ลูกค้าได้รับความพึงพอใจ
                              ประสบการณ์ที่ประทับใจและความมั่นใจในตราสินค้า</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มีกลยุทธ์การสร้างคุณค่าของตราสินค้า (Brand Value)
                              การนำเสนอคุณค่าที่ได้จากตราสินค้าเมื่อเทียบกับราคา
                              เน้นย้ำให้ลูกค้ารับรู้ได้ถึงความคุ้มค่าสมราคา </label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มีกลยุทธ์การเจาะกลุ่มเป้าหมายของ ตราสินค้า (Brand
                              Positioning) การกำหนดกลุ่มเป้าหมายที่ชัดเจน เจาะตลาด เพื่อให้ตราสินค้าเข้าไป
                              เป็นหนึ่งในใจลูกค้า สร้างความผูกพันและภักดีต่อตราสินค้า</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มีกลยุทธ์การสร้างการรับรู้ตราสินค้า (Brand
                              Awareness) การสื่อสารไปยังลูกค้าผ่านการโฆษณาประชาสัมพันธ์ และกิจกรรมทางการตลาด
                              ที่เหมาะสมเพื่อตอกย้ำให้เกิดการรับรู้ตราสินค้า</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BOTTOM -->
            <div class="step-preview-bottom">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-preview-bottom-wrapper">
                    <div class="form-button-step">
                      <a href="step-company-preview.php" class="form-button btn">Prev</a>
                      <a href="step-preview-2.php" class="form-button btn">Next</a>
                    </div>
                    <!-- STEP BUTTON -->
                    <div class="form-button-preview">
                      <a href="step-company.php" class="form-button btn --blue">Edit</a>
                      <a href="" download class="form-button btn --blue"><i><img src="assets/images/icons/icon-download.svg" alt=""></i>Download <span>(PDF)</span></a>
                      <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg"
                              alt=""></i>Submit</button>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>