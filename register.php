<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>Register</title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- REGISTER -->
      <section class="section section-register section-form">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="form-wrapper register-form">
                <!-- HEADLINE -->
                <div class="form-wrapper-headline is-revealing">
                  <h1 class="h3 text-uppercase text-heading-bold">Register</h1>
                </div>
                <!-- REGISTER FORM -->
                <div class="box-form is-revealing">
                  <form class="js-validate" autocomplete="off" enctype="multipart/form-data" novalidate>
                    <div class="register-form-row">
                      <!-- LEFT -->
                      <div class="register-form-col">
                        <div class="box-form-title">
                          <p>To register. you agree with the Terms of use and Privacy policy.</p>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <!-- ROW -->
                          <div class="form-row">
                            <!-- COL -->
                            <div class="form-col --two">
                              <div class="form-container">
                                <label for="" class="form-label">Name</label>
                                <input type="text" class="form-input js-validate--input" placeholder="" value="" required>
                              </div>
                            </div>
                            <!-- COL -->
                            <div class="form-col --two">
                              <div class="form-container">
                                <label for="" class="form-label">Last name</label>
                                <input type="text" class="form-input js-validate--input" placeholder="" value="" required>
                              </div>
                            </div>
                            <!-- COL -->
                            <div class="form-col --two">
                              <div class="form-container">
                                <label for="" class="form-label">Email</label>
                                <input type="email" class="form-input js-validate--input" placeholder="" value="" required>
                              </div>
                            </div>
                            <!-- COL -->
                            <div class="form-col --two">
                              <div class="form-container">
                                <label for="" class="form-label">Telephone</label>
                                <input type="tel" class="form-input js-validate--input" placeholder="" value="" required>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- RIGHT -->
                      <div class="register-form-col register-form-col-right">
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <!-- ROW -->
                          <div class="form-row">
                            <!-- COL -->
                            <div class="form-col">
                              <div class="form-container">
                                <label for="" class="form-label">Username</label>
                                <input type="text" class="form-input js-validate--input" placeholder="" value="" required>
                              </div>
                            </div>
                            <!-- COL -->
                            <div class="form-col">
                              <div class="form-container">
                                <label for="" class="form-label">Password</label>
                                <input id="register-password" type="password" class="form-input js-validate--input" placeholder="" value="" required>
                              </div>
                            </div>
                            <!-- COL -->
                            <div class="form-col">
                              <div class="form-container">
                                <label for="" class="form-label">Confirm Password</label>
                                <input type="password" class="form-input js-validate--input" placeholder="" value="" data-confirm="#register-password" required>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- BUTTON -->
                        <div class="form-button-wrapper">
                          <button type="submit" class="form-button btn --gradient">Submit</button>
                        </div>
                      </div>
                    </div>

                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  
  <script src="assets/scripts/main.js"></script>
</body>

</html>