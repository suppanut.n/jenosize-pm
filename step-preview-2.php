<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">

        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="">
            <!-- STEP HEADLINE -->
            <div class="step-headline-preview">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-preview-wrapper">
                      <div class="preview-logo">
                        <img data-src="assets/images/logos/logo-green.svg" alt="Prime Minister’s Export Award 2018"
                          class="js-imageload-self">
                      </div>
                      <div class="preview-title">
                        <div class="preview-title-award-logo">
                          <img src="assets/images/application/categories/best-green-innovation.svg" alt="Best Green Innovation">
                        </div>
                        <div class="preview-title-text">
                          <h3 class="h2">BEST GREEN INNOVATION</h3>
                        </div>
                      </div>
                      <div class="preview-page">3/6</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP CONTENT 2 (CONTINUE) -->
                    <div class="step">
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การบริหารตราสินค้าในต่างประเทศ (Brand Management)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio" checked>
                            <label for="" class="form-label-radio">มีนโยบาย การวางแผนด้านกลยุทธ์
                              วัตถุประสงค์/เป้าหมายและการควบคุมทางการตลาดที่ ชัดเจนในระยะสั้นและระยะยาว</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio">
                            <label for="" class="form-label-radio">มีการศึกษาตลาดและพฤติกรรมผู้บริโภค
                              และนำข้อมูลมาใช้ประโยชน์</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio">
                            <label for="" class="form-label-radio">มีการแบ่งส่วนตลาด
                              การเลือกตลาดเป้าหมายและการวางตำแหน่งทางการตลาด </label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio">
                            <label for="" class="form-label-radio">มีกลยุทธ์การบริหารการขายและการบริหารความสัมพันธ์กับลูกค้า</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio">
                            <label for="" class="form-label-radio">ผลที่ได้จากความพยายามในการดำเนินกลยุทธ์เพื่อสร้างตราสินค้า
                              (Brand) ในช่วง 3 ปี (พ.ศ. 2557-2559)</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบเอกสาร/หลักฐานประกอบทุกรายการโดยแยกเป็นรายปี</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- FILE -->
                            <div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
<!-- FILE -->
<div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีการจัดทำสื่อประชาสัมพันธ์และกิจกรรมส่งเสริมการขายในต่างประเทศ (เลือกได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการทำธุรกิจออนไลน์ผ่าน e-commerce platform
                              ของตนเอง เช่น website หรือ application ของบริษัท เป็นต้น</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการทำการตลาดและการสื่อสารในรูปแบบดิจิทัล
                              (digital marketing/social media) </label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการจัดทำสื่อประชาสัมพันธ์อื่นๆ เช่น ป้ายโฆษณา
                              รถโฆษณา โบรชัวร์ แผ่นพับ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการจัดกิจกรรมส่งเสริมการค้าหรือเข้าร่วมงานแสดงสินค้าในต่างประเทศ</label>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- STEP CONTENT 3 -->
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 3</h3>
                        <p>การพัฒนาเพื่อขยายฐานลูกค้า</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีการจดทะเบียนเครื่องหมายการค้าในต่างประเทศ</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-1" class="form-radio" checked>
                            <label for="" class="form-label-radio">1-2 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">3-4 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">มากกว่า 5 ประเทศ</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- FILE -->
                            <div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีการนำผลิตภัณฑ์ที่มีเครื่องหมายการค้าของตนเองไปวางจำหน่ายในต่างประเทศ</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-2" class="form-radio" checked>
                            <label for="" class="form-label-radio">1-3 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">4-6 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มากกว่า 6 ประเทศ</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน <span>อาทิ ใบกำกับสินค้า (Invoice) ใบตราส่ง
                            (Bill of Lading) หรือ Airway Bill</span></label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- FILE -->
                            <div class="form-add-file-preview-wrapper">
                              <a href="" download class="form-add-file-preview-item">
                                <div class="form-add-file-icon">
                                  <?php include('inc/icon-file.php'); ?>
                                </div>
                                <div class="form-add-file-text">www.pm-award.com/app/2019/jenosize/file_01.jpg</div>
                              </a>
                            </div>
                            <!-- END FILE -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BOTTOM -->
            <div class="step-preview-bottom">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-preview-bottom-wrapper">
                      <div class="form-button-step">
                        <a href="step-preview-1.php" class="form-button btn">Prev</a>
                        <a href="step-preview-3.php" class="form-button btn">Next</a>
                      </div>
                      <!-- STEP BUTTON -->
                      <div class="form-button-preview">
                        <a href="step-company.php" class="form-button btn --blue">Edit</a>
                        <a href="" download class="form-button btn --blue"><i><img src="assets/images/icons/icon-download.svg"
                              alt=""></i>Download <span>(PDF)</span></a>
                              <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg"
                              alt=""></i>Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>