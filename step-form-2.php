<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="step-form-3.php" novalidate class="js-validate">
            <!-- STEP HEADLINE -->
            <div class="step-headline">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-wrapper">
                      <ul class="step-lists">
                        <li><a href="step-company.php">ข้อมูลบริษัท</a></li>
                        <li class="is--active"><a href="step-form-1.php">คุณสมบัติ</a></li>
                      </ul>
                      <div class="step-headline-status">
                        <div class="application-status --red">
                          <div class="application-status-color"></div>
                          <p class="application-status-text">
                            Uncomplete <span>85</span>%
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- BEGIN STEP PROGRESS -->
            <div class="pm-step js-step-mobile">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="pm-step__current js-step-mobile--trigger">
                      <p class="pm-step__label"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></p>
                      <i>
                        <?php include('inc/svg/icon-arrow-down.php'); ?></i>
                    </div>
                    <ul class="pm-step__content js-step-mobile--content">
                      <li class="is-active"><a href="step-form-1.php"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></a></li>
                      <li class="is-active"><a href="step-form-2.php"><span class="no">หมวดที่ 2</span><span>การวางแผนเชิงกลยุทธ์</span></a></li>
                      <li><a href="step-form-3.php"><span class="no">หมวดที่ 3</span><span>การพัฒนาเพื่อขยายฐานลูกค้า</span></a></li>
                      <li><a href="step-form-4.php"><span class="no">หมวดที่ 4</span><span>การวัดการวิเคราะห์และการจัดการความรู้</span></a></li>
                      <li><a href="step-form-5.php"><span class="no">หมวดที่ 5</span><span>บุคลากร</span></a></li>
                      <li><a href="step-form-6.php"><span class="no">หมวดที่ 6</span><span>การดำเนินงาน</span></a></li>
                      <li><a href="step-form-7.php"><span class="no">หมวดที่ 7</span><span>ความสำเร็จจากการดำเนินธุรกิจ</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- END STEP PROGRESS -->
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step">
                      <!-- FORM HEADLINE -->
                      <div class="form-h">
                        <h3>หมวดที่ 2</h3>
                        <p>การวางแผนเชิงกลยุทธ์</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>แนวคิดในการพัฒนารูปแบบผลิตภัณฑ์ (ครอบคลุมบรรจุภัณฑ์) ในรอบ 1 - 2 ปี ที่ผ่านมา</h4>
                        </div>
                        <div class="form-lists js-validate--group">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">มีหน่วยงาน R&D
                              ทั้งในและนอกสถานที่เพื่อวิจัยและพัฒนาผลิตภัณฑ์/
                              มีการใช้งานวิจัยของนักวิจัยในประเทศหรือต่างประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">มีการพัฒนารูปแบบผลิตภัณฑ์เพื่อมีคุณภาพและมาตรฐานระดับสากล</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">มีการพัฒนารูปแบบผลิตภัณฑ์เพื่อตอบสนองตลาดหรือลูกค้าต่างประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">มีการพัฒนารูปแบบผลิตภัณฑ์เพื่อสิ่งแวดล้อม</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>กลยุทธ์ในการสร้างตราสินค้า</h4>
                        </div>
                        <div class="form-lists js-validate--group">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio" required>
                            <label for="" class="form-label-radio">มีกลยุทธ์การสร้างเอกลักษณ์ของตราสินค้า (Brand
                              Identity) การนำเสนอความเป็นตัวตนที่ชัดเจน
                              และโดดเด่นของสินค้าเพื่อสร้างความแตกต่างจากคู่แข่ง เน้นย้ำให้ ลูกค้าได้รับความพึงพอใจ
                              ประสบการณ์ที่ประทับใจและความมั่นใจในตราสินค้า</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio" required>
                            <label for="" class="form-label-radio">มีกลยุทธ์การสร้างคุณค่าของตราสินค้า (Brand Value)
                              การนำเสนอคุณค่าที่ได้จากตราสินค้าเมื่อเทียบกับราคา
                              เน้นย้ำให้ลูกค้ารับรู้ได้ถึงความคุ้มค่าสมราคา </label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio" required>
                            <label for="" class="form-label-radio">มีกลยุทธ์การเจาะกลุ่มเป้าหมายของ ตราสินค้า (Brand
                              Positioning) การกำหนดกลุ่มเป้าหมายที่ชัดเจน เจาะตลาด เพื่อให้ตราสินค้าเข้าไป
                              เป็นหนึ่งในใจลูกค้า สร้างความผูกพันและภักดีต่อตราสินค้า</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-2" class="form-radio" required>
                            <label for="" class="form-label-radio">มีกลยุทธ์การสร้างการรับรู้ตราสินค้า (Brand
                              Awareness) การสื่อสารไปยังลูกค้าผ่านการโฆษณาประชาสัมพันธ์ และกิจกรรมทางการตลาด
                              ที่เหมาะสมเพื่อตอกย้ำให้เกิดการรับรู้ตราสินค้า</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การบริหารตราสินค้าในต่างประเทศ (Brand Management)</h4>
                        </div>
                        <div class="form-lists js-validate--group">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio" required>
                            <label for="" class="form-label-radio">มีนโยบาย การวางแผนด้านกลยุทธ์
                              วัตถุประสงค์/เป้าหมายและการควบคุมทางการตลาดที่ ชัดเจนในระยะสั้นและระยะยาว</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio" required>
                            <label for="" class="form-label-radio">มีการศึกษาตลาดและพฤติกรรมผู้บริโภค
                              และนำข้อมูลมาใช้ประโยชน์</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio" required>
                            <label for="" class="form-label-radio">มีการแบ่งส่วนตลาด
                              การเลือกตลาดเป้าหมายและการวางตำแหน่งทางการตลาด </label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio" required>
                            <label for="" class="form-label-radio">มีกลยุทธ์การบริหารการขายและการบริหารความสัมพันธ์กับลูกค้า</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-2-radio-3" class="form-radio" required>
                            <label for="" class="form-label-radio">ผลที่ได้จากความพยายามในการดำเนินกลยุทธ์เพื่อสร้างตราสินค้า
                              (Brand) ในช่วง 3 ปี (พ.ศ. 2557-2559)</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบเอกสาร/หลักฐานประกอบทุกรายการโดยแยกเป็นรายปี</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- ADD FILE -->
                            <div class="form-add-file-wrapper js-input-file-dynamic">
                              <div class="js-input-file-dynamic--container">
                                <!-- TEMPLATE ITEM -->
                                <!-- <div class="form-add-file-item js-input-file-dynamic--item">
                                  <input type="file" name="input-file-dynamic-${TIMESTAMP}" placeholder="(jpg.png.pdf 50MB)">
                                  <div class="form-add-file-icon"><span class="icon-svg"> <canvas width="23px" height="30px"></canvas> <svg width="23px" height="30px" viewBox="0 0 23 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M1,28.374 L21.453,28.374 L21.453,1 L7.109,1 L1,7.109 L1,28.374 Z M21.953,29.374 L0.5,29.374 C0.224,29.374 0,29.15 0,28.874 L0,6.902 C0,6.769 0.053,6.642 0.146,6.548 L6.548,0.147 C6.642,0.053 6.769,0 6.901,0 L21.953,0 C22.229,0 22.453,0.224 22.453,0.5 L22.453,28.874 C22.453,29.15 22.229,29.374 21.953,29.374 Z" fill="#FFFFFF"></path> </g> </svg> </span> </div>
                                  <div class="form-add-file-text">${FILENAME}</div>
                                  <div class="form-add-file-delete js-input-file-dynamic--delete"> <a href=""><span class="icon-svg"> <canvas width="16px" height="16px"></canvas> <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <polygon id="Fill-1" fill="#ED1C24" points="2.1213 -0.0001 0.0003 2.1209 13.2083 15.3299 15.3293 13.2079"></polygon> <polygon id="Fill-2" fill="#ED1C24" points="13.2084 -0.0001 0.0004 13.2079 2.1214 15.3299 15.3294 2.1209"></polygon> </g> </svg> </span></a> </div>
                                </div> -->
                              </div>
                              
                              <div class="form-add-file-item form-add-file-item-button">
                                <input type="file" name="input-dynamic" placeholder="(jpg.png.pdf 50MB)" class="js-input-file-dynamic--file">
                                <span class="form-add-file-filename">(Drag Files Here or Browse)</span>
                                <button class="form-button btn --blue form-add-file-button">+ ADD FILE</button>
                              </div>
                            </div>
                            <!-- END ADD FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <div class="form-upload-wrapper">
                              <input type="file" id="" class="form-upload" placeholder="(jpg.png.pdf 50MB)">
                              <span class="form-upload-filename">(jpg.png.pdf 50MB)</span>
                              <button class="form-button btn --gradient form-upload-button">UPLOAD</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีการจัดทำสื่อประชาสัมพันธ์และกิจกรรมส่งเสริมการขายในต่างประเทศ (เลือกได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists js-validate--group">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการทำธุรกิจออนไลน์ผ่าน e-commerce platform
                              ของตนเอง เช่น website หรือ application ของบริษัท เป็นต้น</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการทำการตลาดและการสื่อสารในรูปแบบดิจิทัล
                              (digital marketing/social media) </label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการจัดทำสื่อประชาสัมพันธ์อื่นๆ เช่น ป้ายโฆษณา
                              รถโฆษณา โบรชัวร์ แผ่นพับ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการจัดกิจกรรมส่งเสริมการค้าหรือเข้าร่วมงานแสดงสินค้าในต่างประเทศ</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BUTTON -->
            <div class="">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="form-button-wrapper">
                      <a href="step-form-1.php" class="form-button btn --blue">Prev</a>
                      <button type="submit" class="form-button btn --gradient">SUBMIT</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>