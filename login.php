<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>Login</title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <!-- LOGIN -->
      <section class="section section-login section-form">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="form-wrapper login-form">
                <!-- HEADLINE -->
                <div class="form-wrapper-headline is-revealing">
                  <h1 class="h3 text-uppercase text-heading-bold">Prime Minister’s Export Award Member</h1>
                </div>
                <!-- LOGIN FORM -->
                <div class="box-form is-revealing">
                  <form action="" class="js-validate" novalidate>
                    <div class="box-form-title">
                      <p>Login to your account</p>
                    </div>
                    <!-- FORM GROUP -->
                    <div class="form-group">
                      <!-- ROW -->
                      <div class="form-row">
                        <!-- COL -->
                        <div class="form-col">
                          <div class="form-container">
                            <label for="" class="form-label">Email</label>
                            <input type="email" class="form-input js-validate--input" placeholder="" value="" required>
                          </div>
                        </div>
                        <!-- COL -->
                        <div class="form-col">
                          <div class="form-container">
                            <label for="" class="form-label">Password</label>
                            <input type="password" class="form-input js-validate--input" placeholder="" value="" required>
                          </div>
                        </div>
                        <!-- COL -->
                        <div class="form-col --text">
                          <div class="form-lists">
                            <div class="form-list">
                              <input type="checkbox" class="form-checkbox">
                              <label for="" class="form-label-checkbox">Remember me.</label>
                            </div>
                          </div>
                        </div>
                        <!-- COL -->
                        <div class="form-col --text">
                          <div class="box-form-text-small">
                            <a href="forgot-password.php">Forget password?</a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!-- BUTTON -->
                    <div class="form-button-wrapper">
                      <button type="submit" class="form-button btn --gradient">Login</button>
                    </div>
                    <!-- SIGN UP -->
                    <div class="box-form-text-secondary">
                      <p>Don’t have an account? <a href="register.php">Sign up</a></p>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>