<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title></title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <?php
    $bannerApi = 'api/news-event.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner.php'); ?>
      <?php } ?>
      <!-- NEWS LISTS -->
      <section class="section section-news">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="news">
                <!-- HEADLINE -->
                <div class="news-headline is-revealing">
                  <div class="news-date">
                    <div class="news-headline-result">
                      <div class="news-headline-total text-gradient">440</div>
                      <div class="news-headline-result-text">
                        <div class="news-headline-title">News & Event Update</div>
                        <div class="news-headline-date">Jan 11 , 2018</div>
                      </div>
                    </div>
                  </div>
                  <div class="news-filter-form">
                    <form action="">
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col">
                            <div class="form-container">
                              <div class="select-wrapper">
                                <select class="js-selectric">
                                  <option value="">Category</option>
                                  <option value="1">Category 1</option>
                                  <option value="2">Category 2</option>
                                  <option value="3">Category 3</option>
                                  <option value="4">Category 4</option>
                                </select>
                                <?php include('inc/select-arrow.php'); ?>
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col">
                            <div class="form-container">
                              <input type="search" class="form-input" placeholder="Keyword" value="">
                            </div>
                          </div>
                          <!-- BUTTON -->
                          <div class="form-col form-col-button">
                            <button type="submit" class="form-button btn --gradient">Search</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
                <div class="news-lists">
                  <?php
                $listApi = 'api/news-lists.json'; // path to your JSON file
                $listData = file_get_contents($listApi); // put the contents of the file into a variable
                $listArray = json_decode($listData); // deco√de the JSON feed
                foreach ($listArray as $value) {
                ?>
                  <!-- NEWS <?php echo $value->id ?> -->
                  <div class="news-list is-revealing">
                    <div class="news-box">
                      <div class="news-box-wrapper">
                        <!-- THUMB -->
                        <a href="<?php echo $value->link_url ?>" title="<?php echo $value->title ?>" class="news-box-thumb">
                          <div class="bg">
                            <div class="bg-container">
                              <img data-src="<?php echo $value->thumb_url ?>" alt="" class="js-imageload">
                            </div>
                          </div>
                        </a>
                        <div class="news-box-content">
                          <div class="news-box-content-title">
                            <h3 class="h4">
                              <a href="<?php echo $value->link_url ?>">
                                <?php echo $value->title ?></a>
                            </h3>
                          </div>
                          <div class="news-box-content-desc">
                            <p>
                              <?php echo $value->desc ?>
                            </p>
                          </div>
                          <div class="news-box-content-link">
                            <a href="<?php echo $value->link_url ?>" title="<?php echo $value->title ?>" class="h4 is--line--anim">Read
                              More</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <!-- PAGINATION -->
                <div class="news-pagination pagination is-revealing">
                  <ul>
                    <li class="prev disabled"><a href="news-event.php">Previous</a></li>
                    <li class="active"><a href="news-event.php">1</a></li>
                    <li><a href="news-event.php">2</a></li>
                    <li><a href="news-event.php">3</a></li>
                    <li><a href="news-event.php">4</a></li>
                    <li><a href="news-event.php">5</a></li>
                    <li><a href="news-event.php">6</a></li>
                    <li><a href="news-event.php">7</a></li>
                    <li>...</li>
                    <li><a href="news-event.php">100</a></li>
                    <li class="next"><a href="news-event.php">Next</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('inc/cta.php'); ?>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>