<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="step-form-5.php" novalidate class="js-validate">
            <!-- STEP HEADLINE -->
            <div class="step-headline">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-wrapper">
                      <ul class="step-lists">
                        <li><a href="step-company.php">ข้อมูลบริษัท</a></li>
                        <li class="is--active"><a href="step-form-1.php">คุณสมบัติ</a></li>
                      </ul>
                      <div class="step-headline-status">
                        <div class="application-status --red">
                          <div class="application-status-color"></div>
                          <p class="application-status-text">
                            Uncomplete <span>85</span>%
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- BEGIN STEP PROGRESS -->
            <div class="pm-step js-step-mobile">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="pm-step__current js-step-mobile--trigger">
                      <p class="pm-step__label"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></p>
                      <i>
                        <?php include('inc/svg/icon-arrow-down.php'); ?></i>
                    </div>
                    <ul class="pm-step__content js-step-mobile--content">
                      <li class="is-active"><a href="step-form-1.php"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></a></li>
                      <li class="is-active"><a href="step-form-2.php"><span class="no">หมวดที่ 2</span><span>การวางแผนเชิงกลยุทธ์</span></a></li>
                      <li class="is-active"><a href="step-form-3.php"><span class="no">หมวดที่ 3</span><span>การพัฒนาเพื่อขยายฐานลูกค้า</span></a></li>
                      <li class="is-active"><a href="step-form-4.php"><span class="no">หมวดที่ 4</span><span>การวัดการวิเคราะห์และการจัดการความรู้</span></a></li>
                      <li><a href="step-form-5.php"><span class="no">หมวดที่ 5</span><span>บุคลากร</span></a></li>
                      <li><a href="step-form-6.php"><span class="no">หมวดที่ 6</span><span>การดำเนินงาน</span></a></li>
                      <li><a href="step-form-7.php"><span class="no">หมวดที่ 7</span><span>ความสำเร็จจากการดำเนินธุรกิจ</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- END STEP PROGRESS -->
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 4</h3>
                        <p>การวัดการวิเคราะห์และการจัดการความรู้</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การจัดการสารสนเทศและองค์ความรู้ (สามารถระบุได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists js-validate--group">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการบันทึกขั้นตอนการผลิตผลิตภัณฑ์</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการถ่ายทอดความเป็นมาของผลิตภัณฑ์ให้เป็นที่รู้จักต่อภายในองค์กรหรือชุมชน</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีกระบวนการสืบทอด/รักษา/ส่งเสริมการสร้างองค์ความรู้เกี่ยวกับผลิตภัณฑ์ต่อสมาชิกภายในองค์กรหรือชุมชน</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการเปิดให้เยี่ยมชมสถานที่ผลิตผลิตภัณฑ์แก่บุคคลทั่วไปทั้งคนไทยและคนต่างประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" required>
                            <label for="" class="form-label-checkbox">มีการแลกเปลี่ยนองค์ความรู้กับชุมชน ธุรกิจ
                              หรือหน่วยงานที่เกี่ยวข้อง</label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BUTTON -->
            <div class="">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="form-button-wrapper">
                      <a href="step-form-3.php" class="form-button btn --blue">Prev</a>
                      <button type="submit" class="form-button btn --gradient">Next</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>