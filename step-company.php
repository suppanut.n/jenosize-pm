<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="step-form-1.php" novalidate class="js-validate">
            <!-- STEP HEADLINE -->
            <div class="step-headline">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-wrapper">
                      <ul class="step-lists">
                        <li class="is--active"><a href="step-company.php">ข้อมูลบริษัท</a></li>
                        <li><a href="step-form-1.php">คุณสมบัติ</a></li>
                      </ul>
                      <div class="step-headline-status">
                        <div class="application-status --red">
                          <div class="application-status-color"></div>
                          <p class="application-status-text">
                            Uncomplete <span>85</span>%
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP -->
                    <div class="step">
                      <div class="form-block-wrapper">
                        <!-- FORM BLOCK -->
                        <div class="form-block">
                          <div class="form-block-title">
                            <h4>คำแนะนำ</h4>
                          </div>
                          <div class="form-block-desc">
                            <ul>
                              <li>เพื่อผลประโยชน์ของท่าน กรุณาอธิบายรายละเอียดในแต่ละข้อให้ชัดเจน
                                ถูกต้องครบถ้วนและสมบูรณ์ที่สุด</li>
                              <li>กรุณาให้ข้อมูลทุกข้อ ถ้าข้อใดท่านไม่มีข้อมูลหรือไม่เกี่ยวข้องกับท่าน ให้ระบุว่า
                                "ไม่มี"
                                หรือ "ไม่เกี่ยวข้อง" (ห้ามเว้นช่องว่างไว้โดยเด็ดขาด)</li>
                              <li>ในกรณีที่ช่องว่างที่จัดไว้ให้ลงข้อมูลไม่เพียงพอ
                                ท่านสามารถแนบเอกสารเพื่ออธิบายรายละเอียดเพิ่มเติมได้</li>
                              <li>หากมีการสืบทราบภายหลังว่า ข้อมูลที่ท่านให้มาไม่เป็นความจริง
                                ท่านจะถูกเพิกถอนสิทธิ์ในการรับรางวัลและหากปรากฏข้อเท็จจริงในภายหลังจากที่ได้รับรางวัลแล้วว่าท่านมีการกระทำ/ดำเนินการใดๆ
                                ที่ส่ง ผลเสียหายและ/หรือ
                                ผลกระทบต่อเกียรติภูมิและศักดิ์ศรีแห่งรางวัลผู้ประกอบธุรกิจดีเด่น
                                คณะกรรมการมีสิทธิ์โดยชอบธรรมที่จะเรียกคืนซึ่งรางวัลดังกล่าวและยกเลิกสิทธิประโยชน์ที่พึงได้รับโดยทันที</li>
                            </ul>
                          </div>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <label for="" class="form-label">ชื่อบริษัท</label>
                          <div class="form-input-row">
                            <div class="form-input-col --full">
                              <input type="text" name="company-name-th" class="form-input js-validate--input" placeholder="(ภาษาไทย)" required>
                            </div>
                            <div class="form-input-col --full">
                              <input type="text" name="company-name-en" class="form-input js-validate--input" placeholder="(ภาษาอังกฤษ)" required>
                            </div>
                          </div>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <label for="" class="form-label">หมายเลขทะเบียนนิติบุคคล</label>
                          <div class="form-input-row">
                            <div class="form-input-col">
                              <input type="text" id="" class="form-input js-validate--input" placeholder="(ภาษาไทย)" required>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ที่อยู่บริษัท</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">เลขที่:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">หมู่ที่:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตรอก/ซอย:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ถนน:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">แขวง/ตำบล:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">เขต/อำเภอ:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">จังหวัด:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">รหัสไปรษณีย์:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรสาร:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">Website:</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">เครื่องหมายการค้า</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <div class="form-upload-wrapper">
                              <input type="file" id="" name="" class="form-upload" placeholder="(jpg.png.pdf 50MB)" required>
                              <span class="form-upload-filename js-validate--input">(jpg.png.pdf 50MB)</span>
                              <button class="form-button btn --gradient form-upload-button">UPLOAD</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">ประเภทสินค้า</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- ADD FILE -->
                            <div class="form-add-file-wrapper js-input-file-dynamic">
                              <div class="js-input-file-dynamic--container">
                                <!-- TEMPLATE ITEM -->
                                <!-- <div class="form-add-file-item js-input-file-dynamic--item">
                                  <input type="file" name="input-file-dynamic-${TIMESTAMP}" placeholder="(jpg.png.pdf 50MB)">
                                  <div class="form-add-file-icon"><span class="icon-svg"> <canvas width="23px" height="30px"></canvas> <svg width="23px" height="30px" viewBox="0 0 23 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M1,28.374 L21.453,28.374 L21.453,1 L7.109,1 L1,7.109 L1,28.374 Z M21.953,29.374 L0.5,29.374 C0.224,29.374 0,29.15 0,28.874 L0,6.902 C0,6.769 0.053,6.642 0.146,6.548 L6.548,0.147 C6.642,0.053 6.769,0 6.901,0 L21.953,0 C22.229,0 22.453,0.224 22.453,0.5 L22.453,28.874 C22.453,29.15 22.229,29.374 21.953,29.374 Z" fill="#FFFFFF"></path> </g> </svg> </span> </div>
                                  <div class="form-add-file-text">${FILENAME}</div>
                                  <div class="form-add-file-delete js-input-file-dynamic--delete"> <a href=""><span class="icon-svg"> <canvas width="16px" height="16px"></canvas> <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <polygon id="Fill-1" fill="#ED1C24" points="2.1213 -0.0001 0.0003 2.1209 13.2083 15.3299 15.3293 13.2079"></polygon> <polygon id="Fill-2" fill="#ED1C24" points="13.2084 -0.0001 0.0004 13.2079 2.1214 15.3299 15.3294 2.1209"></polygon> </g> </svg> </span></a> </div>
                                </div> -->
                              </div>
                              
                              <div class="form-add-file-item form-add-file-item-button">
                                <input type="file" name="input-dynamic" placeholder="(jpg.png.pdf 50MB)" class="js-input-file-dynamic--file">
                                <span class="form-add-file-filename">(Drag Files Here or Browse)</span>
                                <button class="form-button btn --blue form-add-file-button">+ ADD FILE</button>
                              </div>
                            </div>
                            <!-- END ADD FILE -->
                          </div>
                        </div>
                      </div>
                      <div class="form-block-wrapper">
                        <!-- FORM BLOCK -->
                        <div class="form-block">
                          <div class="form-block-desc">
                            <p>นิยามศัพท์ การแบ่งขนาดบริษัทเป็น 3 ขนาด ประกอบด้วยขนาดเล็ก ขนาดกลาง ขนาดใหญ่
                              (ตามประกาศกฎกระทรวงอุตสาหกรรม เรื่อง กำหนดจำนวนการจ้างงานและมูลค่าสินทรัพย์ถาวรของ
                              วิสาหกิจขนาดกลาง และขนาดย่อม พ.ศ. 2545) ดังนี้
                              1.บริษัทขนาดเล็ก (S) หมายถึง กิจการที่มีจำนวนการจ้างงานไม่เกิน 50 คน
                              หรือมีมูลค่าสินทรัพย์ถาวรสุทธิ (ไม่รวมที่ดิน) ไม่เกิน 50 ล้านบาท
                              2.บริษัทขนาดกลาง (M) หมายถึง กิจการที่มีจำนวนการจ้างงานไม่เกิน 200 คน
                              หรือมีมูลค่าสินทรัพย์
                              ถาวรสุทธิ (ไม่รวมที่ดิน) ไม่เกิน 200 ล้านบาท
                              3.บริษัทขนาดใหญ่ (L) หมายถึง กิจการที่มีจำนวนการจ้างงานเกิน 200 คน
                              หรือมีมูลค่าสินทรัพย์ถาวรสุทธิ (ไม่รวมที่ดิน) เกิน 200 ล้านบาท</p>
                          </div>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <!-- TITLE -->
                          <div class="form-group-title">
                            <h4>ขนาดของกิจการ</h4>
                          </div>
                          <div class="form-lists --inline --large">
                            <div class="form-list">
                              <input type="radio" id="" name="step-company-radio-1" class="form-radio">
                              <label for="" class="form-label-radio">ขนาดเล็ก</label>
                            </div>
                            <div class="form-list">
                              <input type="radio" id="" name="step-company-radio-1" class="form-radio">
                              <label for="" class="form-label-radio">ขนาดกลาง</label>
                            </div>
                            <div class="form-list">
                              <input type="radio" id="" name="step-company-radio-1" class="form-radio">
                              <label for="" class="form-label-radio">ขนาดใหญ่</label>
                            </div>
                          </div>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <label for="" class="form-label">มูลค่าสินทรัพย์ถาวรสุทธิ (ไม่รวมที่ดิน)</label>
                          <div class="form-input-row">
                            <div class="form-input-col">
                              <input type="text" id="" class="form-input" placeholder="(บาท)">
                            </div>
                          </div>
                        </div>
                        <!-- FORM GROUP -->
                        <div class="form-group">
                          <label for="" class="form-label">จำนวนการจ้างงาน</label>
                          <div class="form-input-row">
                            <div class="form-input-col">
                              <input type="text" id="" class="form-input" placeholder="(คน)">
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>เจ้าหน้าที่ประสานงาน</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ชื่อ</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตำแหน่ง</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ชื่อ</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตำแหน่ง</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ผู้มีอำนาจลงนามของบริษัท</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ชื่อ</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">ตำแหน่ง</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">โทรศัพท์</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --four">
                            <label for="" class="form-label">E-mail</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(กรุณาระบุ)">
                              </div>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BUTTON -->
            <div class="">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="form-button-wrapper">
                      <a href="my-application-data.php" class="form-button btn --blue">Prev</a>
                      <button class="form-button btn --gradient">Next</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>