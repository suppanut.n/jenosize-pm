<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="" novalidate class="js-validate">
            <!-- STEP HEADLINE -->
            <div class="step-headline">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-wrapper">
                      <ul class="step-lists">
                        <li><a href="step-company.php">ข้อมูลบริษัท</a></li>
                        <li class="is--active"><a href="step-form-1.php">คุณสมบัติ</a></li>
                      </ul>
                      <div class="step-headline-status">
                        <div class="application-status --red">
                          <div class="application-status-color"></div>
                          <p class="application-status-text">
                            Uncomplete <span>85</span>%
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- BEGIN STEP PROGRESS -->
            <div class="pm-step js-step-mobile">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="pm-step__current js-step-mobile--trigger">
                      <p class="pm-step__label"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></p>
                      <i>
                        <?php include('inc/svg/icon-arrow-down.php'); ?></i>
                    </div>
                    <ul class="pm-step__content js-step-mobile--content">
                      <li class="is-active"><a href="step-form-1.php"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></a></li>
                      <li class="is-active"><a href="step-form-2.php"><span class="no">หมวดที่ 2</span><span>การวางแผนเชิงกลยุทธ์</span></a></li>
                      <li class="is-active"><a href="step-form-3.php"><span class="no">หมวดที่ 3</span><span>การพัฒนาเพื่อขยายฐานลูกค้า</span></a></li>
                      <li class="is-active"><a href="step-form-4.php"><span class="no">หมวดที่ 4</span><span>การวัดการวิเคราะห์และการจัดการความรู้</span></a></li>
                      <li class="is-active"><a href="step-form-5.php"><span class="no">หมวดที่ 5</span><span>บุคลากร</span></a></li>
                      <li class="is-active"><a href="step-form-6.php"><span class="no">หมวดที่ 6</span><span>การดำเนินงาน</span></a></li>
                      <li class="is-active"><a href="step-form-7.php"><span class="no">หมวดที่ 7</span><span>ความสำเร็จจากการดำเนินธุรกิจ</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- END STEP PROGRESS -->
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 7</h3>
                        <p>ความสำเร็จจากการดำเนินธุรกิจ</p>
                      </div>

                      <!-- FORM GROUP 3 -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>ร้อยละของยอดส่งออกสินค้าที่สมัครต่อยอดจำหน่ายทั้งหมดภายในและต่างประเทศ (เฉลี่ย 3
                            ปีย้อนหลัง)</h4>
                        </div>
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ปี 2561</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัคร</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดจำหน่ายรวม</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ปี 2560</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัคร</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดจำหน่ายรวม</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ปี 2559</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัคร</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดจำหน่ายรวม</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>

                          <!-- COL -->
                          <div class="form-col --three">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัครเฉลี่ย 3 ปี (ก)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --three">
                            <label for="" class="form-label">ยอดจำหน่ายรวมเฉลี่ย 3 ปี (ข)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --three">
                            <label for="" class="form-label">ร้อยละ (ก) / (ข)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การเพิ่มขึ้นของยอดส่งออกสินค้าที่สมัครขอรับรางวัล (Brand’s export growth)
                            คำนวณโดยใช้ข้อมูล
                            ปี 2559, 2560, 2561 เทียบกับปีก่อนหน้า</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">ยอดส่งออกสินค้าที่สมัครเพิ่มขึ้นติดต่อกัน 2 ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">ยอดส่งออกสินค้าที่สมัครเพิ่มขึ้น 1 ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">ยอดส่งออกสินค้าที่สมัครคงที่ หรือไม่เพิ่มขึ้น</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การเพิ่มขึ้นของยอดส่งออกสินค้าที่สมัครขอรับรางวัล (Brand’s growth: export + domestic)
                            คำนวณโดยใช้ข้อมูล ปี 2559, 2560, 2561 เทียบกับปีก่อนหน้า</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ยอดจำหน่ายรวมของสินค้าที่สมัครเพิ่มขึ้นติดต่อกัน 2
                              ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ยอดจำหน่ายรวมของสินค้าที่สมัครเพิ่มขึ้น 1 ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ยอดจำหน่ายรวมของสินค้าที่สมัครคงที่
                              หรือไม่เพิ่มขึ้น</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มาตรฐานคุณภาพสินค้าที่ได้รับ (เลือกได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox">
                            <label for="" class="form-label-checkbox">มาตรฐานคุณภาพผลิตภัณฑ์ขั้นพื้นฐานในประเทศที่ออกโดยหน่วยงานของเอกชน
                              เช่น มาตรฐานของมูลนิธิสิ่งแวดล้อมไทย</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox">
                            <label for="" class="form-label-checkbox">ได้รับมาตรฐานคุณภาพผลิตภัณฑ์ขั้นพื้นฐานในประเทศที่ออกโดยหน่วยงานของรัฐ
                              เช่น GMP, อย.อย.</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox">
                            <label for="" class="form-label-checkbox">มาตรฐานของสมาคม/องค์กร/หน่วยธุรกิจในประเทศต่างๆ
                              เช่น มาตรฐานของสมาคมค้าปลีก</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox">
                            <label for="" class="form-label-checkbox">มาตรฐานระดับสากล เช่น ISO, HACCP เป็นต้น</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน พร้อม หน้าข้อที่มีหลักฐาน)
                          สำเนาใบรับรองมาตรฐานต่าง ๆ ที่ได้รับ</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- ADD FILE -->
                            <div class="form-add-file-wrapper js-input-file-dynamic">
                              <div class="js-input-file-dynamic--container">
                                <!-- TEMPLATE ITEM -->
                                <!-- <div class="form-add-file-item js-input-file-dynamic--item">
                                  <input type="file" name="input-file-dynamic-${TIMESTAMP}" placeholder="(jpg.png.pdf 50MB)">
                                  <div class="form-add-file-icon"><span class="icon-svg"> <canvas width="23px" height="30px"></canvas> <svg width="23px" height="30px" viewBox="0 0 23 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M1,28.374 L21.453,28.374 L21.453,1 L7.109,1 L1,7.109 L1,28.374 Z M21.953,29.374 L0.5,29.374 C0.224,29.374 0,29.15 0,28.874 L0,6.902 C0,6.769 0.053,6.642 0.146,6.548 L6.548,0.147 C6.642,0.053 6.769,0 6.901,0 L21.953,0 C22.229,0 22.453,0.224 22.453,0.5 L22.453,28.874 C22.453,29.15 22.229,29.374 21.953,29.374 Z" fill="#FFFFFF"></path> </g> </svg> </span> </div>
                                  <div class="form-add-file-text">${FILENAME}</div>
                                  <div class="form-add-file-delete js-input-file-dynamic--delete"> <a href=""><span class="icon-svg"> <canvas width="16px" height="16px"></canvas> <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <polygon id="Fill-1" fill="#ED1C24" points="2.1213 -0.0001 0.0003 2.1209 13.2083 15.3299 15.3293 13.2079"></polygon> <polygon id="Fill-2" fill="#ED1C24" points="13.2084 -0.0001 0.0004 13.2079 2.1214 15.3299 15.3294 2.1209"></polygon> </g> </svg> </span></a> </div>
                                </div> -->
                              </div>
                              
                              <div class="form-add-file-item form-add-file-item-button">
                                <input type="file" name="input-dynamic" placeholder="(jpg.png.pdf 50MB)" class="js-input-file-dynamic--file">
                                <span class="form-add-file-filename">(Drag Files Here or Browse)</span>
                                <button class="form-button btn --blue form-add-file-button">+ ADD FILE</button>
                              </div>
                            </div>
                            <!-- END ADD FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title --line text-center">
                          <h4>ขอรับรองว่าทั้งหมดเป็นความจริง</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ลายมือชื่อ (ผู้มีอำนาจลงนามของบริษัท)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input js-validate--input" placeholder="(กรุณาระบุ)" required>
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --six">
                            <label for="" class="form-label">วัน</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <div class="select-wrapper js-validate--input">
                                  <select class="js-selectric" required>
                                    <option value="">(วว)</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                    <option value="3">03</option>
                                    <option value="4">04</option>
                                    <option value="5">05</option>
                                    <option value="6">06</option>
                                    <option value="7">07</option>
                                    <option value="8">08</option>
                                    <option value="9">09</option>
                                    <option value="10">10</option>
                                    <option value="11">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                    <option value="30">30</option>
                                    <option value="31">31</option>
                                  </select>
                                  <?php include('inc/select-arrow.php'); ?>
                                </div>
                              </div>
                            </div>
                          </div>

                          <!-- COL -->
                          <div class="form-col --six">
                            <label for="" class="form-label">เดือน</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <div class="select-wrapper js-validate--input">
                                  <select class="js-selectric" required>
                                    <option value="">(ดด)</option>
                                    <option value="มกราคม">มกราคม</option>
                                    <option value="กุมภาพันธ์">กุมภาพันธ์</option>
                                    <option value="มีนาคม">มีนาคม</option>
                                    <option value="เมษายน">เมษายน</option>
                                    <option value="พฤษภาคม">พฤษภาคม</option>
                                    <option value="มิถุนายน">มิถุนายน</option>
                                    <option value="กรกฎาคม">กรกฎาคม</option>
                                    <option value="สิงหาคม">สิงหาคม</option>
                                    <option value="กันยายน">กันยายน</option>
                                    <option value="ตุลาคม">ตุลาคม</option>
                                    <option value="พฤศจิกายน">พฤศจิกายน</option>
                                    <option value="ธันวาคม">ธันวาคม</option>
                                  </select>
                                  <?php include('inc/select-arrow.php'); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --six">
                            <label for="" class="form-label">ปี</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <div class="select-wrapper js-validate--input">
                                  <select class="js-selectric" required>
                                    <option value="">(ปป)</option>
                                    <option value="2561">2561</option>
                                    <option value="2562">2562</option>
                                    <option value="2563">2563</option>
                                    <option value="2564">2564</option>
                                  </select>
                                  <?php include('inc/select-arrow.php'); ?>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BUTTON -->
            <div class="">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="form-button-wrapper">
                      <a href="step-company-preview.php" class="form-button btn --blue"><i><img src="assets/images/icons/icon-search.svg"
                            alt=""></i>Preview</a>
                      <button type="submit" class="form-button btn --gradient js-modal--trigger" data-id="#success-modal"><i><img src="assets/images/icons/icon-letter.svg" alt=""></i>Submit</button>
                      <!-- <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg" alt=""></i>Submit</button> -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  
  <?php include('inc/modal-success.php'); ?>
  <!-- NOTE : open modal by script 
  js_modal.openModal('#success-modal'); -->

  <script src="assets/scripts/main.js"></script>
</body>

</html>