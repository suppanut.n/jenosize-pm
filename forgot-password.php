<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>Forgot password</title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- LOGIN -->
      <section class="section section-login section-form">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="form-wrapper login-form">
                <!-- HEADLINE -->
                <div class="form-wrapper-headline is-revealing">
                  <h1 class="h3 text-uppercase text-heading-bold">Forgot password</h1>
                </div>
                <!-- LOGIN FORM -->
                <div class="box-form is-revealing">
                  <form action="" class="js-validate" novalidate>
                    <div class="box-form-title">
                      <p>Forgot your password? Please enter your email address. You will receive a link to create a new
                        password via email.</p>
                    </div>
                    <!-- FORM GROUP -->
                    <div class="form-group">
                      <!-- ROW -->
                      <div class="form-row">
                        <!-- COL -->
                        <div class="form-col">
                          <div class="form-container">
                            <label for="" class="form-label">Email</label>
                            <input type="email" class="form-input js-validate--input" placeholder="" value="" required>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- BUTTON -->
                    <div class="form-button-wrapper">
                      <button type="submit" class="form-button btn --gradient">Reset password</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>