<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-application">
        <div class="fixed-layout">
          <div class="section-outer">
            <div class="section-inner">
              <div class="application">
                <!-- APPLICATION HEADLINE -->
                <div class="application-headline is-revealing">
                  <div class="application-headline-row">
                    <div class="application-headline-title">
                      <h2 class="h4 text-heading-bold text-uppercase">My Application</h2>
                    </div>
                    <div class="application-headline-filter">
                      <div class="select-wrapper">
                        <select class="js-selectric">
                          <option value="2019">2019</option>
                          <option value="2018">2018</option>
                          <option value="2017">2017</option>
                          <option value="2016">2016</option>
                        </select>
                        <div class="select-arrow">
                          <svg width="16px" height="9px" viewBox="0 0 16 9" version="1.1" xmlns="http://www.w3.org/2000/svg"
                            xmlns:xlink="http://www.w3.org/1999/xlink">
                            <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <g id="down-arrow" fill="currentColor">
                                <path d="M15.7675214,0.218803419 C15.5487179,0 15.1931624,0 14.974359,0.218803419 L8,7.20683761 L1.01196581,0.218803419 C0.793162393,0 0.437606838,0 0.218803419,0.218803419 C-2.49800181e-16,0.437606838 -2.49800181e-16,0.793162393 0.218803419,1.01196581 L7.58974359,8.38290598 C7.6991453,8.49230769 7.83589744,8.54700855 7.98632479,8.54700855 C8.12307692,8.54700855 8.27350427,8.49230769 8.38290598,8.38290598 L15.7538462,1.01196581 C15.9863248,0.793162393 15.9863248,0.437606838 15.7675214,0.218803419 Z"
                                  id="Path"></path>
                              </g>
                            </g>
                          </svg>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- APPLICATION LISTS -->
                <div class="application-lists">
                  <?php
                $popularApi = 'api/application-lists.json'; // path to your JSON file
                $popularData = file_get_contents($popularApi); // put the contents of the file into a variable
                $popularArray = json_decode($popularData); // decode the JSON feed
                foreach ($popularArray as $value) {
                  if($value->active === true) {
                    $active = 'is--active';
                  } else {
                    $active = '';
                  }
              ?>
                  <?php include('inc/my-application-lists.php'); ?>
                  <?php } ?>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <?php include('inc/modal.php'); ?>
  <script src="assets/scripts/main.js"></script>
</body>

</html>