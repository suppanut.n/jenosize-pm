<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="step-form-4.php" novalidate class="js-validate">
            <!-- STEP HEADLINE -->
            <div class="step-headline">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-wrapper">
                      <ul class="step-lists">
                        <li><a href="step-company.php">ข้อมูลบริษัท</a></li>
                        <li class="is--active"><a href="step-form-1.php">คุณสมบัติ</a></li>
                      </ul>
                      <div class="step-headline-status">
                        <div class="application-status --red">
                          <div class="application-status-color"></div>
                          <p class="application-status-text">
                            Uncomplete <span>85</span>%
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- BEGIN STEP PROGRESS -->
            <div class="pm-step js-step-mobile">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="pm-step__current js-step-mobile--trigger">
                      <p class="pm-step__label"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></p>
                      <i>
                        <?php include('inc/svg/icon-arrow-down.php'); ?></i>
                    </div>
                    <ul class="pm-step__content js-step-mobile--content">
                      <li class="is-active"><a href="step-form-1.php"><span class="no">หมวดที่ 1</span><span>การบริหารองค์กร</span></a></li>
                      <li class="is-active"><a href="step-form-2.php"><span class="no">หมวดที่ 2</span><span>การวางแผนเชิงกลยุทธ์</span></a></li>
                      <li class="is-active"><a href="step-form-3.php"><span class="no">หมวดที่ 3</span><span>การพัฒนาเพื่อขยายฐานลูกค้า</span></a></li>
                      <li><a href="step-form-4.php"><span class="no">หมวดที่ 4</span><span>การวัดการวิเคราะห์และการจัดการความรู้</span></a></li>
                      <li><a href="step-form-5.php"><span class="no">หมวดที่ 5</span><span>บุคลากร</span></a></li>
                      <li><a href="step-form-6.php"><span class="no">หมวดที่ 6</span><span>การดำเนินงาน</span></a></li>
                      <li><a href="step-form-7.php"><span class="no">หมวดที่ 7</span><span>ความสำเร็จจากการดำเนินธุรกิจ</span></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <!-- END STEP PROGRESS -->
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 3</h3>
                        <p>การพัฒนาเพื่อขยายฐานลูกค้า</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีการจดทะเบียนเครื่องหมายการค้าในต่างประเทศ</h4>
                        </div>
                        <div class="form-lists js-validate--group">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">1-2 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">3-4 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-1" class="form-radio" required>
                            <label for="" class="form-label-radio">มากกว่า 5 ประเทศ</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน</label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- ADD FILE -->
                            <div class="form-add-file-wrapper js-input-file-dynamic">
                              <div class="js-input-file-dynamic--container">
                                <!-- TEMPLATE ITEM -->
                                <!-- <div class="form-add-file-item js-input-file-dynamic--item">
                                  <input type="file" name="input-file-dynamic-${TIMESTAMP}" placeholder="(jpg.png.pdf 50MB)">
                                  <div class="form-add-file-icon"><span class="icon-svg"> <canvas width="23px" height="30px"></canvas> <svg width="23px" height="30px" viewBox="0 0 23 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M1,28.374 L21.453,28.374 L21.453,1 L7.109,1 L1,7.109 L1,28.374 Z M21.953,29.374 L0.5,29.374 C0.224,29.374 0,29.15 0,28.874 L0,6.902 C0,6.769 0.053,6.642 0.146,6.548 L6.548,0.147 C6.642,0.053 6.769,0 6.901,0 L21.953,0 C22.229,0 22.453,0.224 22.453,0.5 L22.453,28.874 C22.453,29.15 22.229,29.374 21.953,29.374 Z" fill="#FFFFFF"></path> </g> </svg> </span> </div>
                                  <div class="form-add-file-text">${FILENAME}</div>
                                  <div class="form-add-file-delete js-input-file-dynamic--delete"> <a href=""><span class="icon-svg"> <canvas width="16px" height="16px"></canvas> <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <polygon id="Fill-1" fill="#ED1C24" points="2.1213 -0.0001 0.0003 2.1209 13.2083 15.3299 15.3293 13.2079"></polygon> <polygon id="Fill-2" fill="#ED1C24" points="13.2084 -0.0001 0.0004 13.2079 2.1214 15.3299 15.3294 2.1209"></polygon> </g> </svg> </span></a> </div>
                                </div> -->
                              </div>
                              
                              <div class="form-add-file-item form-add-file-item-button">
                                <input type="file" name="input-dynamic" placeholder="(jpg.png.pdf 50MB)" class="js-input-file-dynamic--file">
                                <span class="form-add-file-filename">(Drag Files Here or Browse)</span>
                                <button class="form-button btn --blue form-add-file-button">+ ADD FILE</button>
                              </div>
                            </div>
                            <!-- END ADD FILE -->
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>มีการนำผลิตภัณฑ์ที่มีเครื่องหมายการค้าของตนเองไปวางจำหน่ายในต่างประเทศ</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">1-3 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">4-6 ประเทศ</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-3-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">มากกว่า 6 ประเทศ</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <label for="" class="form-label">โปรดแนบหลักฐาน <span>อาทิ ใบกำกับสินค้า (Invoice) ใบตราส่ง
                            (Bill of Lading) หรือ Airway Bill</span></label>
                        <div class="form-input-row">
                          <div class="form-input-col">
                            <!-- ADD FILE -->
                            <div class="form-add-file-wrapper js-input-file-dynamic">
                              <div class="js-input-file-dynamic--container">
                                <!-- TEMPLATE ITEM -->
                                <!-- <div class="form-add-file-item js-input-file-dynamic--item">
                                  <input type="file" name="input-file-dynamic-${TIMESTAMP}" placeholder="(jpg.png.pdf 50MB)">
                                  <div class="form-add-file-icon"><span class="icon-svg"> <canvas width="23px" height="30px"></canvas> <svg width="23px" height="30px" viewBox="0 0 23 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <path d="M1,28.374 L21.453,28.374 L21.453,1 L7.109,1 L1,7.109 L1,28.374 Z M21.953,29.374 L0.5,29.374 C0.224,29.374 0,29.15 0,28.874 L0,6.902 C0,6.769 0.053,6.642 0.146,6.548 L6.548,0.147 C6.642,0.053 6.769,0 6.901,0 L21.953,0 C22.229,0 22.453,0.224 22.453,0.5 L22.453,28.874 C22.453,29.15 22.229,29.374 21.953,29.374 Z" fill="#FFFFFF"></path> </g> </svg> </span> </div>
                                  <div class="form-add-file-text">${FILENAME}</div>
                                  <div class="form-add-file-delete js-input-file-dynamic--delete"> <a href=""><span class="icon-svg"> <canvas width="16px" height="16px"></canvas> <svg width="16px" height="16px" viewBox="0 0 16 16" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"> <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <polygon id="Fill-1" fill="#ED1C24" points="2.1213 -0.0001 0.0003 2.1209 13.2083 15.3299 15.3293 13.2079"></polygon> <polygon id="Fill-2" fill="#ED1C24" points="13.2084 -0.0001 0.0004 13.2079 2.1214 15.3299 15.3294 2.1209"></polygon> </g> </svg> </span></a> </div>
                                </div> -->
                              </div>
                              
                              <div class="form-add-file-item form-add-file-item-button">
                                <input type="file" name="input-dynamic" placeholder="(jpg.png.pdf 50MB)" class="js-input-file-dynamic--file">
                                <span class="form-add-file-filename">(Drag Files Here or Browse)</span>
                                <button class="form-button btn --blue form-add-file-button">+ ADD FILE</button>
                              </div>
                            </div>
                            <!-- END ADD FILE -->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BUTTON -->
            <div class="">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="form-button-wrapper">
                      <a href="step-form-2.php" class="form-button btn --blue">Prev</a>
                      <button type="submit" class="form-button btn --gradient">Next</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>