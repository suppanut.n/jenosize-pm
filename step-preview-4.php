<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title>My Application</title>
</head>

<body>
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header-2.php'); ?>
    <main class="site-main">
      <!-- BANNER -->
      <?php
    $bannerApi = 'api/application-banner.json'; // path to your JSON file
    $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
    $bannerArray = json_decode($bannerData); // decode the JSON feed
    foreach ($bannerArray as $value) {
    ?>
      <?php include('inc/banner-small.php'); ?>
      <?php } ?>
      <!-- APPLICATION -->
      <section class="section section-step">
        <!-- APPLICATION FORM -->
        <div class="application-form">
          <form action="">
            <!-- STEP HEADLINE -->
            <div class="step-headline-preview">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-headline-preview-wrapper">
                      <div class="preview-logo">
                        <img data-src="assets/images/logos/logo-green.svg" alt="Prime Minister’s Export Award 2018"
                          class="js-imageload-self">
                      </div>
                      <div class="preview-title">
                        <div class="preview-title-award-logo">
                          <img src="assets/images/application/categories/best-green-innovation.svg" alt="Best Green Innovation">
                        </div>
                        <div class="preview-title-text">
                          <h3 class="h2">BEST GREEN INNOVATION</h3>
                        </div>
                      </div>
                      <div class="preview-page">5/6</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP CONTENT -->
            <div class="step-content">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <!-- STEP CONTENT 6 -->
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 6</h3>
                        <p>การดำเนินงาน</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การควบคุมคุณภาพผลิตภัณฑ์ ครอบคลุมบรรจุภัณฑ์ (สามารถระบุได้มากกว่า 1 ข้อ)</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการตรวจสอบคุณภาพวัตถุดิบ (Incoming
                              Inspection) ด้วยเครื่องมือหรือเทคนิคหรือภูมิปัญญาที่น่าเชื่อถือ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการตรวจสอบคุณภาพผลิตภัณฑ์ระหว่างการผลิต (In
                              Process Inspection) ด้วยเครื่องมือหรือเทคนิคหรือภูมิปัญญาที่น่าเชื่อถือ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการตรวจสอบคุณภาพผลิตภัณฑ์ขั้นสุดท้าย (Final
                              Inspection) ด้วยเครื่องมือหรือเทคนิคหรือภูมิปัญญาที่น่าเชื่อถือ</label>
                          </div>
                          <div class="form-list">
                            <input type="checkbox" id="" name="checkbox-1" class="form-checkbox" checked>
                            <label for="" class="form-label-checkbox">มีการบันทึกข้อมูลเพื่อตรวจสอบย้อนหลัง</label>
                          </div>
                        </div>
                      </div>

                    </div>
                    <!-- STEP CONTENT 7 -->
                    <div class="step">
                      <div class="form-h">
                        <h3>หมวดที่ 7</h3>
                        <p>ความสำเร็จจากการดำเนินธุรกิจ</p>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>ร้อยละของยอดส่งออกสินค้าที่สมัครต่อยอดจำหน่ายทั้งหมดภายในและต่างประเทศ (เฉลี่ย 3
                            ปีย้อนหลัง)</h4>
                        </div>
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ปี 2561</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัคร</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดจำหน่ายรวม</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ปี 2560</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัคร</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดจำหน่ายรวม</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- TITLE -->
                        <div class="form-group-title --line">
                          <h4>ปี 2559</h4>
                        </div>
                        <!-- ROW -->
                        <div class="form-row">
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัคร</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --two">
                            <label for="" class="form-label">ยอดจำหน่ายรวม</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>

                          <!-- COL -->
                          <div class="form-col --three">
                            <label for="" class="form-label">ยอดส่งออกสินค้าที่สมัครเฉลี่ย 3 ปี (ก)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --three">
                            <label for="" class="form-label">ยอดจำหน่ายรวมเฉลี่ย 3 ปี (ข)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                          <!-- COL -->
                          <div class="form-col --three">
                            <label for="" class="form-label">ร้อยละ (ก) / (ข)</label>
                            <div class="form-input-row">
                              <div class="form-input-col">
                                <input type="text" id="" class="form-input" placeholder="(บาท)" value="1,000,000 บาท">
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การเพิ่มขึ้นของยอดส่งออกสินค้าที่สมัครขอรับรางวัล (Brand’s export growth)
                            คำนวณโดยใช้ข้อมูล
                            ปี 2559, 2560, 2561 เทียบกับปีก่อนหน้า</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-2" class="form-radio" checked>
                            <label for="" class="form-label-radio">ยอดส่งออกสินค้าที่สมัครเพิ่มขึ้นติดต่อกัน 2 ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">ยอดส่งออกสินค้าที่สมัครเพิ่มขึ้น 1 ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-2" class="form-radio">
                            <label for="" class="form-label-radio">ยอดส่งออกสินค้าที่สมัครคงที่ หรือไม่เพิ่มขึ้น</label>
                          </div>
                        </div>
                      </div>
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <!-- TITLE -->
                        <div class="form-group-title">
                          <h4>การเพิ่มขึ้นของยอดส่งออกสินค้าที่สมัครขอรับรางวัล (Brand’s growth: export + domestic)
                            คำนวณโดยใช้ข้อมูล ปี 2559, 2560, 2561 เทียบกับปีก่อนหน้า</h4>
                        </div>
                        <div class="form-lists">
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-1" class="form-radio" checked>
                            <label for="" class="form-label-radio">ยอดจำหน่ายรวมของสินค้าที่สมัครเพิ่มขึ้นติดต่อกัน 2
                              ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ยอดจำหน่ายรวมของสินค้าที่สมัครเพิ่มขึ้น 1 ปี</label>
                          </div>
                          <div class="form-list">
                            <input type="radio" id="" name="step-form-7-radio-1" class="form-radio">
                            <label for="" class="form-label-radio">ยอดจำหน่ายรวมของสินค้าที่สมัครคงที่
                              หรือไม่เพิ่มขึ้น</label>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- STEP BOTTOM -->
            <div class="step-preview-bottom">
              <div class="fixed-layout">
                <div class="section-outer">
                  <div class="section-inner">
                    <div class="step-preview-bottom-wrapper">
                    <div class="form-button-step">
                      <a href="step-preview-3.php" class="form-button btn">Prev</a>
                      <a href="step-preview-5.php" class="form-button btn">Next</a>
                    </div>
                    <!-- STEP BUTTON -->
                    <div class="form-button-preview">
                      <a href="step-company.php" class="form-button btn --blue">Edit</a>
                      <a href="" download class="form-button btn --blue"><i><img src="assets/images/icons/icon-download.svg" alt=""></i>Download <span>(PDF)</span></a>
                      <button type="submit" class="form-button btn --gradient"><i><img src="assets/images/icons/icon-letter.svg"
                              alt=""></i>Submit</button>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </section>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>
  <script src="assets/scripts/main.js"></script>
</body>

</html>