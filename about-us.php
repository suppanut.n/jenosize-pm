<!doctype html>
<html lang="en">

<head>
  <?php include('inc/meta.php'); ?>
  <title></title>
</head>

<body class="has-animations">
  <?php include('inc/loader.php'); ?>
  <div class="site-global">
    <?php include('inc/header.php'); ?>
    <main class="site-main">
      <?php
      $bannerApi = 'api/about-us.json'; // path to your JSON file
      $bannerData = file_get_contents($bannerApi); // put the contents of the file into a variable
      $bannerArray = json_decode($bannerData); // decode the JSON feed
      foreach ($bannerArray as $value) {
      ?>
      <?php include('inc/banner.php'); ?>
      <?php } ?>
      <section class="section section-about">
        <!-- ABOUT -->
        <div class="about">
          <div class="fixed-layout">
            <div class="section-outer">
              <div class="about-wrapper">
                <div class="about-left">

                  <div class="about-award is-revealing">
                    <span class="icon-svg">
                      <canvas width="227px" height="595px"></canvas>
                      <img data-src="assets/images/about/about/award.png" alt="" class="js-imageload-self">
                    </span>
                  </div>
                  <!-- HEADLINE -->
                  <div class="about-headline is-revealing">
                    <h2 class="h1">About prime minister’s export award</h2>
                  </div>
                </div>

                <!-- CONTENT -->
                <div class="about-content is-revealing">
                  <div class="about-content-wrapper">
                    <div class="about-title">
                      <h3 class="h2">ปี ๒๕๓๕ ในสมัยนายกรัฐมนตรีอานันท์ ปันยารชุน
                        กระทรวงพาณิชย์ได้จัดให้มีการพิจารณารางวัล
                        ผู้ส่งออกสินค้าและบริการดีเด่น</h3>
                    </div>
                    <div class="about-desc">
                      <p>(Prime Minister’s Export Award หรือ PM Export Award) เป็นครั้งแรก
                        เพื่อแสดงถึงภาพลักษณ์ของคุณภาพและมาตรฐานของสินค้าไทยในตลาดโลกโดยมีเป้าหมายที่จะสนับสนุนและให้ความสำคัญแก่ผู้ส่งออกสินค้าและบริการที่มีผลงานดีเด่น
                        มีการริเริ่มและพยายามบุกเบิกตลาดต่างประเทศ</p>

                      <p>ภายใต้ชื่อทางการค้าของตนเอง และมีการออกแบบผลิตภัณฑ์ของตนเองจนเป็นที่ยอมรับในตลาดโลก
                        รวมทั้งสนับสนุนการสร้างภาพลักษณ์องค์กรที่ดีให้แก่ผู้ประกอบการที่มีส่วนผลักดันการส่งออกสินค้าและบริการของประเทศ
                        โดยรางวัลดังกล่าวถือเป็นรางวัลสูงสุดของรัฐบาลที่มอบให้แก่ผู้ส่งออกซึ่งรัฐบาลได้กำหนดให้มีพิธีประกาศเกียรติคุณ
                        และมอบรางวัลดังกล่าวอย่างต่อเนื่องจนถึงปัจจุบัน (ปี ๒๕๖๐) เป็นปีที่ ๒๖
                        โดยมีนายกรัฐมนตรีเป็นประธานการมอบรางวัลทุกครั้งเพื่อเป็นเกียรติแก่ผู้ประกอบการในการพัฒนาสินค้าและบริการให้มีคุณภาพได้มาตรฐานเป็นที่ยอมรับในระดับสากล
                        จนสามารถแข่งขันในตลาดต่างประเทศได้อย่างยั่งยืน
                        ส่งผลให้การส่งออกสินค้าและบริการมีการขยายตัวเพิ่มขึ้น ทั้งในด้านปริมาณและมูลค่ารวม
                        ทั้งเป็นแหล่งสร้างรายได้หลักเข้าประเทศ</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- OBJECTIVE -->
        <div class="objective">
          <!-- BG -->
          <div class="objective-bg bg">
            <div class="bg-container">
              <img data-src="assets/images/about/objective/bg.jpg" alt="" class="js-imageload">
            </div>
          </div>
          <div class="fixed-layout">
            <div class="section-outer">
              <div class="objective-wrapper is-revealing-inner">
                <!-- HEADLINE -->
                <div class="objective-headline">
                  <div class="objective-headline-title">
                    <h2 class="h1">Objective prime minister’s export award</h2>
                  </div>
                </div>
                <!-- CONTENT -->
                <div class="objective-content">
                  <p>เพื่อกระตุ้นให้มีการพัฒนารูปแบบสินค้าที่เป็น ของผู้ผลิต/ผู้ส่งออกและออกแบบโดยคนไทย
                    รวมทั้งสร้างชื่อเสียงทางการค้าให้เป็นที่รู้จักมากยิ่งขึ้น
                    ในตลาดต่างประเทศเพื่อกระตุ้นให้ผู้ผลิต/ผู้ส่งออกสร้างภาพลักษณ์ที่ดีขององค์กรอันจะนำไปสู่การเสริมสร้างภาพลักษณ์สินค้าและบริการส่งออกของไทยมากขึ้น</p>
                  <p>เพื่อสนับสนุนให้ผู้ประกอบการของไทย เปิดตลาดต่างประเทศโดยใช้ชื่อทางการค้าของตนเอง</p>
                  <p>เพื่อให้สินค้าและบริการของไทยสามารถแข่งขันและรักษาตลาดต่างประเทศได้อย่างยั่งยืน
                    และเป็นที่รู้จักในตลาดโลกมากยิ่งขึ้น เพื่อสนับสนุนให้ผู้ประกอบการของไทย
                    เปิดตลาดต่างประเทศโดยใช้ชื่อทางการค้าของตนเอง</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- RESULTS -->
        <div class="results">
          <div class="fixed-layout">
            <div class="section-outer">
              <!-- HEADLINE -->
              <div class="results-headline is-revealing">
                <h2 class="h4 text-uppercase text-heading-bold">The results are expected to get</h2>
              </div>
              <!-- CONTENT -->
              <div class="results-lists is-revealing-inner">
                <!-- 1 -->
                <div class="results-list">
                  <div class="results-box">
                    <!-- ICON -->
                    <div class="results-box-icon">
                      <img data-src="assets/images/about/results/icon-stats.svg" alt="" class="js-imageload-self">
                    </div>
                    <!-- TEXT -->
                    <div class="results-box-content">
                      <span>1</span>
                      <p>ยกระดับภาพลักษณ์ประเทศด้วยสินค้า และบริการของไทย ให้เป็นที่รู้จักในวงกว้างทั้งในและต่างประเทศ</p>
                    </div>
                  </div>
                </div>
                <!-- 2 -->
                <div class="results-list">
                  <div class="results-box">
                    <!-- ICON -->
                    <div class="results-box-icon">
                      <img data-src="assets/images/about/results/icon-people.svg" alt="" class="js-imageload-self">
                    </div>
                    <!-- TEXT -->
                    <div class="results-box-content">
                      <span>2</span>
                      <p>สร้างกระแสบริโภคนิยมสินค้าไทย ให้เกิดขึ้น ในหมู่ชาวโลก
                        ผ่านแบรนด์ไทยชั้นนำที่ได้รับรางวัลคุณภาพระดับประเทศ ส่งผลให้มูลค่าการส่งออกสินค้าไทยเพิ่มขึ้น
                        อย่างต่อเนื่องและมั่นคง</p>
                    </div>
                  </div>
                </div>
                <!-- 3 -->
                <div class="results-list">
                  <div class="results-box">
                    <!-- ICON -->
                    <div class="results-box-icon">
                      <img data-src="assets/images/about/results/icon-reputation.svg" alt="" class="js-imageload-self">
                    </div>
                    <!-- TEXT -->
                    <div class="results-box-content">
                      <span>3</span>
                      <p>ผู้ประกอบการไทยพัฒนารูปแบบ และคุณภาพสินค้า และสามารถสร้างชื่อเสียง
                        ให้เป็นที่รู้จักในต่างประเทศ</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- BENEFITS -->
        <div class="benefits">
          <div class="fixed-layout">
            <div class="benefits-row">
              <!-- BENEFITS BG -->
              <div class="benefits-bg bg">
                <div class="bg-container">
                  <img data-src="assets/images/about/benefits/bg.jpg" alt="" class="js-imageload">
                </div>
              </div>
              <!-- BENEFITS CONTENT -->
              <div class="benefits-content">
                <!-- BENEFITS HEADLINE -->
                <div class="benefits-headline">
                  <h2 class="h2">10 สิทธิ์ ประโยชน์แก่ผู้ได้รับรางวัล PM Award</h2>
                </div>

                <div class="benefits-blocks">
                  <div class="benefits-block">
                    <!-- BENEFITS LISTS -->
                    <div class="benefits-lists is-revealing-inner">
                      <div class="benefits-list benefits-title">
                        <h3 class="h4">สิทธิประโยชน์ด้านการรับโล่รางวัล/เกียรติบัตรจะได้รับ โล่รางวัล
                          และเกียรติบัตรจากนายกรัฐมนตรี</h3>
                      </div>
                      <div class="benefits-list benefits-title">
                        <h3 class="h4">สิทธิประโยชน์ด้านการเข้าร่วมกิจกรรมกรมฯ</h3>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>1</span>
                        <p>ได้รับการคัดเลือกให้นำสินค้ามาจัดแสดงในงาน
                          นิทรรศการ ของกรมส่งเสริมการค้าระหว่างประเทศ ทั้ง
                          ในและต่าง ประเทศ</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>2</span>
                        <p>ได้รับการพิจารณาเข้าร่วมการจัดกิจกรรมต่างๆ เป็น
                          ลำดับแรก อาทิ การจัด In-store Promotion ร่วมกับ
                          ห้างสรรพสินค้าในต่าง ประเทศ คณะผู้แทนการค้า
                          ระดับสูงเยือนต่างประเทศและการส่งเสริมผู้ประกอบการ
                          ไทย ไปดำเนินธุรกิจในต่างประเทศ</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>3</span>
                        <p>ได้รับส่วนลดค่าใช้จ่ายสมทบการเข้าร่วมงานแสดงสินค้าในต่างประเทศ
                          ของกรมส่งเสริมการค้าระหว่างประเทศ
                          ร้อยละ ๕๐ (ผู้ที่ได้รับรางวัลปี ๒๕๖๐ จะได้รับสิทธิ์ใน ปีงบประมาณ ๒๕๖๑ ระยะเวลาตั้งแต่ ๑
                          ตุลาคม
                          ๒๕๖๐ – ๓๐ กันยายน ๒๕๖๑)</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>4</span>
                        <p>ได้รับการสนับสนุนเข้าร่วมกิจกรรมพัฒนาผู้ ส่งออก ในรูปแบบ การเข้าร่วมสัมมนาการฝึก
                          อบรมเชิงลึกโดยไม่เสียค่าใช้จ่าย</p>
                      </div>
                    </div>
                  </div>
                  <div class="benefits-block">
                    <!-- BENEFITS LISTS -->
                    <div class="benefits-lists is-revealing-inner">
                      
                      <div class="benefits-list benefits-desc">
                        <span>5</span>
                        <p>การได้รับข้อมูลข่าวสารทางการตลาดจาก กรมเป็นประจำทาง mobile application “DITP Connect”</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>6</span>
                        <p>ได้เข้าร่วมเป็นสมาชิกผู้ได้รับรางวัล “PM Award Membership List” ซึ่งต่อไปกรมจะ
                          จัดกิจกรรมการตลาดให้เป็นพิเศษเฉพาะ</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>7</span>
                        <p>สมาชิกกลุ่ม หรือจัดโซนพิเศษให้ในกิจกรรมที่กรมจัด</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>8</span>
                        <p>ได้รับการเผยแพร่ประชาสัมพันธ์ในฐานะผู้ได้รับรางวัลในสื่อ ต่างๆ อย่างต่อเนื่อง อาทิ หนังสือ
                          PM’s
                          Award Directory,นิตยสาร,</p>
                      </div>
                      <div class="benefits-list benefits-title">
                        <h3 class="h4">สิทธิประโยชน์ด้านการประชาสัมพันธ์</h3>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>9</span>
                        <p>หนังสือพิมพ์ และสื่อต่างๆ ทั้งในประเทศและต่างประเทศ</p>
                      </div>
                      <div class="benefits-list benefits-desc">
                        <span>10</span>
                        <p>สามารถใช้ตราสัญลักษณ์ PM Award เพื่อส่งเสริมการขายโดยไม่เสียค่าใช้จ่าย</p>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </section>
      <?php include('inc/cta.php'); ?>
    </main>
    <?php include('inc/footer.php'); ?>
  </div>

  <script src="assets/scripts/main.js"></script>
</body>

</html>